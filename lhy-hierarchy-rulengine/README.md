
https://www.cnblogs.com/rongfengliang/p/12686702.html

### 业务背景介绍

业务系统在应用过程中，常常包含着要处理"复杂、多变"的部分，这部分往往是"业务规则"或者是"数据的处理逻辑"。因此这部分的动态规则的问题，往往需要可配置，并对系统性能和热部署有一定的要求。从开发与业务的视角主要突出以下的一些问题：


- 规则引擎由推理引擎发展而来，是一种嵌入在应用程序中的组件，实现了将业务决策从应用程序代码中分离出来，并使用预定义的语义模块编写业务决策。

- 规则引擎具体执行可以分为接受数据输入，解释业务规则，根据业务规则做出业务决策几个过程。

#### 什么是规则引擎

规则引擎是根据一些算法执行规则的一些列软件系统。规则引擎整合了传入系统的Fact集合和规则集合，从而去触发一个或多个业务操作。规则通常以声明式的方式在业务代码中实现，我们可能以为它很少会被改变。但事实上，这些业务逻辑的判断条件经常会被改变。 业务逻辑或规则，通常是可以表示为“在某写条件下，执行某些任务”。

在拥有大量规则和Fact对象的业务系统中，可能会出现多个Fact输入都会导致同样的输出，这种情况我们通常称作规则冲突。规则引擎可以采用不同的冲突解决方案来确定冲突规则的执行顺序。在规则引擎中，通常有两种执行方式：

1. 正向链接：这是一种基于“数据驱动”的形式，基于插入的Fact对象和Fact对象的更新，规则引擎利用可用的Fact推理规则来提取出更多的Fact对象，直到计算出最终目标，最终会有一个或多个规则被匹配，并计划执行。因此，规则引擎始于事实，始于结论。

2. 反向链接：这是一种基于“目标驱动”或推理形式，与正向链接相反。反向链条从规则引擎假设的结论开始，如果不能够直接满足这些假设，则搜索可满足假设的子目标。规则引擎会循环执行这一过程，直到证明结论或没有更多可证明的子目标为止。


#### 规则引擎的核心概念

- Rule:由条件和行动构成的推理语句，一般表示为IF <conditions> THEN <actions>, Rule表达逻辑。一个规则的IF部分称为LHS，THEN部分称为RHS。

  - Condition：（Left Hand Side）LHS，即条件分支逻辑

  - Action：（Right Hand Side）RHS，即执行逻辑

- Fact：用户输入的事实对象，作为决策因子使用

- RulesEngine：引擎执行器，一般为推理引擎。Rules使用LHS与事实进行模式匹配。当匹配被找到，Rules会执行RHS即执行逻辑，同时actions经常会改变facts的状态，来推理出期望的结果。

- Output：结果对象，规则处理完毕后的结果。



#### 规则引擎与流程处理的区别

规则与过程的不同之处主要在于：业务流程代表业务做了什么；规则引擎代表决定做什么业务。


##### 一个规则文件是由两个概念组成：

> 规则：用于控制业务流程的声明式语句。一个规则通常包含判断条件部分和执行操作部分。如果条件评估结果为true，则执行规则引擎操作部分。
Fact：规则执行所需要的数据。在上面的示例中Account便是Fact对象。

#### 规则引擎的优势

使用规则引擎可以给系统带来如下优势：

- 高灵活性：在规则保存在知识库中，可以在规则变动轻易做出修改。
- 容易掌控：规则比过程代码更易于理解，因此可以有效地来弥补业务分析师和开发人员之间的沟通问题。
- 降低复杂度：在程序中编写大量的判断条件，很可能是会造成一场噩梦。使用规则引擎却能够通过一致的表示形式，更好的处理日益复杂的业务逻辑。
- 可重用性：规则集中管理，可提高业务的规则的可重用性。而且，传统的代码程序通常会添加不必要的变数，很然进行重复利用。


> 在业务规则没有太多变动，业务规则比较简单的情况下，是没有必要使用规则引擎的。


### 常见的规则引擎

开源的代表是Drools，商业的代表是VisualRules ,iLog。

Java 开源的规则引擎有：Drools、URule、Easy Rules。使用最为广泛 Drools。

通过类型对规则引擎进行简单分类，一般有以下3类：

- 通过界面配置的成熟规则引擎：这种规则引擎相对来说就比较重，但是因为功能全，也有部分业务会选择这个，比较出名的有:Drools、URule 
- 基于JVM脚本语言：这种其实不是一个成熟的规则引擎，应该算是规则引擎中的核心技术，因为Drools这种相对太重了，很多互联网公司会基于一些jvm的脚本语言，开发一个轻量级的规则引擎，比较出名的有：Groovy、AviatorScript、QLExpress。
- 基于java代码的规则引擎：上面是基于jvm脚本语言去做的，会有一些语法学习的成本，所以就有基于java代码去做的规则引擎，比如通过一些注解实现抽象的方式去做到规则的扩展，比较出名的有: Easy Rules


#### 规则引擎对比

##### Drools

> Drools 是用 Java 语言编写的开放源码规则引擎，基于Apache协议，基于RETE算法，于2005年被JBoss收购。

##### 特性：

- 简化系统架构，优化应用。 
- 提高系统的可维护性和维护成本。 
- 方便系统的整合。 
- 减少编写“硬代码”业务规则的成本和风险。

那就是只用到Drools的核心BRE，引入几个Maven依赖，编写Java代码和规则文件即可。但是如果要编排很复杂的工程，甚至整个业务都重度依赖，需要产品、运营同学一起来指定规则，则需要用到BRMS整套解决方案了，包括Drools Expert(BRE)、Drools Workbench、DMN。

- Drools太重了，主要是在说：
  - Drools相关的组件太多，需要逐个研究才知道是否需要
  - Drools逻辑复杂，不了解原理，一旦出现问题排查难度高
  - Drools需要编写规则文件，学习成本高

- Drools 官网 : https://www.drools.org
- Drools 中文网 : http://www.drools.org.cn
- Drools GitHub : https://github.com/kiegroup/drools

##### URule

> URule是一款纯Java规则引擎，提供基于Apache协议开源版本和商业版本，它以RETE算法为基础，提供了向导式规则集、脚本式规则集、决策表、交叉决策表(PRO版提供)、决策树、评分卡及决策流共六种类型的规则定义方式，配合基于WEB的设计器，可快速实现规则的定义、维护与发布。

###### URule的优点：

- 向导式规则配置：可以通过web页面配置规则，不依赖开发人员即可配置规则
- 决策表、决策树：提供更加友好的、直观的、支持复杂的规则配置
- 快速测试：在页面可快速构建规则测试
- 集成方便

###### 缺点：

> 开源版本提供的功能有限，无高级功能

- URule 官网 : http://www.bstek.com
- URule GitHub: https://github.com/youseries/urule


##### Easy Rules

Easy Rules 是一款 Java 规则引擎，它的诞生启发自有Martin Fowler 一篇名为 “Should I use a Rules Engine?” 文章。Easy Rules 提供了规则抽象来创建带有条件和操作的规则，以及运行一组规则来评估条件和执行操作的RulesEngine API。

- Easy Rules是一个简单而强大的Java规则引擎，提供以下功能：
  -轻量级框架和易于学习的API
  - 基于POJO的开发与注解的编程模型
  - 定义抽象的业务规则并轻松应用它们
  - 支持从简单规则创建组合规则的能力
  - 支持使用表达式语言（如MVEL和SpEL）定义规则的能力

> Easy Rules GitHub : https://github.com/j-easy/easy-rules

##### 基于Groovy实现轻量级规则引擎

- Groovy是动态语言，依靠反射方式动态执行表达式的求值，并且依靠JIT编译器，在执行次数够多以后，编译成本地字节码，因此性能非常的高，适应于反复执行的表达式，用Groovy脚本动态调整线上代码，无须发版。

- Groovy编译器先将.groovy文件编译成.class文件，然后调用JVM执行*.class文件，可以在Java项目中集成Groovy并充分利用Groovy的动态功能；

- Groovy兼容几乎所有的java语法，开发者完全可以将Groovy当做Java来开发，甚至可以不使用Groovy的特有语法，仅仅通过引入Groovy并使用它的动态能力；

- Groovy可以直接调用项目中现有的Java类(通过import导入)，通过构造函数构造对象并直接调用其方法并返回结果。

  - Groovy 官网 : http://www.groovy-lang.org
  - Groovy GitHub : https://github.com/groovy

##### AviatorScript

Aviator是一个高性能、轻量级的java语言实现的表达式求值引擎，主要用于各种表达式的动态求值。现在已经有很多开源可用的java表达式求值引擎，为什么还需要Avaitor呢？

Aviator的设计目标是轻量级和高性能 ，相比于Groovy、JRuby的笨重，Aviator非常小，加上依赖包也才450K,不算依赖包的话只有70K；当然，Aviator的语法是受限的，它不是一门完整的语言，而只是语言的一小部分集合。

其次，Aviator的实现思路与其他轻量级的求值器很不相同，其他求值器一般都是通过解释的方式运行，而Aviator则是直接将表达式编译成Java字节码，交给JVM去执行。

支持大部分运算操作符，包括算术操作符、关系运算符、逻辑操作符、位运算符、正则匹配操作符(=~)、三元表达式?: ，并且支持操作符的优先级和括号强制优先级，具体请看后面的操作符列表。

###### Aviator的限制：

没有if else、do while等语句，没有赋值语句，仅支持逻辑表达式、算术表达式、三元表达式和正则匹配。

不支持八进制数字字面量，仅支持十进制和十六进制数字字面量。

- AviatorScript 文档 : https://www.yuque.com/boyan-avfmj/aviatorscript

- AviatorScript GitHub : https://github.com/killme2008/aviatorscript

##### QLExpress

> QLExpress从一开始就是从复杂的阿里电商业务系统出发，并且不断完善的脚本语言解析引擎框架，在不追求java语法的完整性的前提下（比如异常处理，foreach循环，lambda表达式，这些都是Groovy的强项），定制了很多普遍存在的业务需求解决方案（比如变量解析，spring打通，函数封装，操作符定制，宏替换），同时在高性能、高并发、线程安全等方面也下足了功夫，久经考验。

- QLExpress GitHub : https://github.com/alibaba/QLExpress

> 参考 : https://developer.aliyun.com/article/621206


#### 参考资料

- https://blog.csdn.net/u013255226/article/details/121086766
- https://blog.csdn.net/weixin_43861115/article/details/125186010
- https://my.oschina.net/u/4072299/blog/4997379
- https://baijiahao.baidu.com/s?id=1718543657537933098&wfr=spider&for=pc
- https://zhuanlan.zhihu.com/p/91158525
- https://blog.csdn.net/weixin_43410352/article/details/119925582
- https://blog.csdn.net/tianshan2010/article/details/104026051
- https://blog.csdn.net/qq_41896365/article/details/122472213
- https://blog.csdn.net/qq_31142553/article/details/85013989?utm_medium=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-5.channel_param&depth_1-utm_source=distribute.pc_relevant.none-task-blog-BlogCommendFromMachineLearnPai2-5.channel_param