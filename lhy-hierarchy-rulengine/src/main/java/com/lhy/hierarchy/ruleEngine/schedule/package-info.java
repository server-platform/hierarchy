/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.schedule
 * @author:LiBo/Alex
 * @create-date:2022-08-14 13:23
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
package com.lhy.hierarchy.ruleEngine.schedule;


/**
 *
 Cron表达式中的星期的正确的配置

 最近再做一个定时任务的需求，使用了Quartz框架，使用了Cron表达式做时间配置，

 做一个定时任务，要求在周一至周五上午05:00点生成表单并完成通知。

 初始表达式为0 0 5 ? * 1-5   （但是这样是错误的）

 错误现象：明明设置的是周一至周五生成，但是周五却没有触发定时任务。

 后查找源码发现，周一不是1   周日才是1   犯了一个挺低级的错误吧 。

 正确表达式：0 0 5 ? * 2-6  或者 0 0 5 ? * MON-FRI （Cron支持配置星期缩写 MON TUE WED THU FRI SAT SUN）

 对于星期的操作，最好可以通过缩写来完成，更加清晰，

 使用数值的话，很容易认为应该从周一开始

 星期数值枚举如下

 public final static int SUNDAY = 1;
 public final static int MONDAY = 2;
 public final static int TUESDAY = 3;
 public final static int WEDNESDAY = 4;
 public final static int THURSDAY = 5;
 public final static int FRIDAY = 6;
 public final static int SATURDAY = 7;
 *
 *
 *
 */