package com.lhy.hierarchy.ruleEngine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 动态规则组信息
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("dynamic_rule_group")
public class DynamicRuleGroupEntity extends Model<DynamicRuleGroupEntity> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 规则引擎id
     */
    @TableField("rule_engine_id")
    private Long ruleEngineId;

    /**
     * 复杂的规则类型-1 简单类型-0
     */
    @TableField("rule_type")
    private Integer ruleType;

    /**
     * 规则名称
     */
    @TableField("name")
    private String name;

    /**
     * 描述信息
     */
    @TableField("description")
    private String description;

    /**
     * 优先级执行
     */
    @TableField("priority")
    private Integer priority;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 是否禁用
     */
    @TableField("is_disable")
    private Integer isDisable;

    /**
     * 删除状态
     */
    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
