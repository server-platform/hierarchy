package com.lhy.hierarchy.ruleEngine.mapper;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleEngineEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 规则引擎信息 Mapper 接口
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Mapper
public interface IDynamicRuleEngineDao extends BaseMapper<DynamicRuleEngineEntity> {

}
