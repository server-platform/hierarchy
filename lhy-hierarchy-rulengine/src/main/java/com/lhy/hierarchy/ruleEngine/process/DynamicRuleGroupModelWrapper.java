/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy.hierarchy.ruleEngine.process;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.process
 * @author:LiBo/Alex
 * @create-date:2022-08-14 13:38
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 *
 * 复合规则一定是由多个简单规则来实现的，对于通道规则而言单通道下发与多通道下发也是依赖于不同场景。
 * 每个单一通道其实我们可以看作是一个规则，将多个规则嵌套在一个复合规则里来实现。
 * 而复合规则可能遵循着不同的实现来决策最终结果。
 * EasyRules实现了 group
 *
 * ActivationRuleGroup - 优先激活规则组
 * ConditionalRuleGroup - 条件规则组
 * UnitRuleGroup - 单元规则组。
 *
 * 我们实现ChannelRule继承了ActivationRuleGroup的复合规则组，
 * ChannelPushRule和ChannelLetterRule等（其他通道规则省略）为组成的简单规则。
 *
 *
 *   UnitRuleGroup:单元规则组是作为一个单元的复合规则:要么应用所有规则，要么什么都不应用。
 *
 *       public static void main(String[] args) {
 *         List<Result> list = new ArrayList<>();
 *         Result p1 = new Result();
 *         p1.setAge(11);
 *         p1.setName("张三");
 *         Result p2 = new Result();
 *         p2.setAge(12);
 *         p2.setName("李四");
 *         list.add(p1);
 *         list.add(p2);
 *         String condition = "person.name contains(\"张\")";
 *         String condition2 = "person.age > 10 && person.age <= 20";
 *         RulesEngine rulesEngine = new DefaultRulesEngine();
 *         Rules rules = new Rules();
 *
 *         Rule rule = new MVELRule().when(condition).then("System.out.println(\"name success\")").priority(2);
 *         Rule rule2 = new MVELRule().when(condition2).then("System.out.println(\"age success\")").priority(1);
 *
 *         // 要么应用所有规则，要么什么都不应用。
 *         UnitRuleGroup unitRuleGroup = new UnitRuleGroup();
 *         unitRuleGroup.addRule(rule); unitRuleGroup.addRule(rule2);
 *         rules.register(unitRuleGroup);
 *
 *         for (int i = 0; i < list.size(); i++) {
 *             Facts facts = new Facts();
 *             facts.put("person", list.get(i));
 *             rulesEngine.fire(rules, facts);
 *         }
 *     }
 *
 *
 *   ActivationRuleGroup:激活规则组是一个复合规则，它触发第一个适用规则，并忽略组中的其他规则(XOR逻辑)。规则首先按照组内的自然顺序(默认优先级)进行排序。
 *   ConditionalRuleGroup:条件规则组是一个复合规则，其中优先级最高的规则充当条件:如果优先级最高的规则求值为true，则触发其余规则。
 *
 *
 */
public class DynamicRuleGroupModelWrapper {
}
