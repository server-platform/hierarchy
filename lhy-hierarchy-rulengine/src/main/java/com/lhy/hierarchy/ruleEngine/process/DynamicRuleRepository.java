/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy.hierarchy.ruleEngine.process;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleEngineEntity;
import com.lhy.hierarchy.ruleEngine.enums.StatusEnum;
import com.lhy.hierarchy.ruleEngine.service.MPDynamicRuleEngineRepository;
import org.jeasy.rules.support.RuleDefinition;
import org.jeasy.rules.support.reader.RuleDefinitionReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Reader;
import java.util.List;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.process
 * @author:LiBo/Alex
 * @create-date:2022-08-13 22:49
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 动态规则处理器
 */
@Component
public class DynamicRuleRepository implements RuleDefinitionReader {


    @Autowired
    MPDynamicRuleEngineRepository mpDynamicRuleEngineRepository;

    /**
     * 查询相关的数据信息
     * @param reader
     * @return
     * @throws Exception
     */
    @Override
    public List<RuleDefinition> read(Reader reader) throws Exception {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("is_disable", StatusEnum.NO.getStatus());
        queryWrapper.eq("is_delete", StatusEnum.NO.getStatus());
        List<DynamicRuleEngineEntity> dataList
                = mpDynamicRuleEngineRepository.list(queryWrapper);
        if(CollectionUtil.isNotEmpty(dataList)){

        }
        return null;
    }

}
