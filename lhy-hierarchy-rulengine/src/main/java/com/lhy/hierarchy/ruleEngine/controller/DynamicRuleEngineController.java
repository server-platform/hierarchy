package com.lhy.hierarchy.ruleEngine.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 规则引擎信息 前端控制器
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@RestController
@RequestMapping("/dynamic/rule/engine")
public class DynamicRuleEngineController {

}
