/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy.hierarchy.ruleEngine.process;

import cn.hutool.core.collection.CollectionUtil;
import com.google.common.base.Splitter;
import com.lhy.hierarchy.ruleEngine.model.DynamicRuleEngineModel;
import com.lhy.hierarchy.ruleEngine.model.DynamicRuleModel;
import org.apache.commons.lang3.StringUtils;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.core.RuleBuilder;
import org.jeasy.rules.mvel.MVELAction;
import org.jeasy.rules.mvel.MVELCondition;

import java.util.List;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.process
 * @author:LiBo/Alex
 * @create-date:2022-08-13 22:37
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class DynamicRuleModelWrapper {



    Splitter splitter = Splitter.on(",").trimResults();

    /**
     * wrapper包装方法
     * @param dynamicRuleEngineModel
     * @return
     */
    public Rule wrapper(DynamicRuleModel dynamicRuleEngineModel){
        RuleBuilder ruleBuilder =new RuleBuilder()
                .name(dynamicRuleEngineModel.getRuleName())
                .description(dynamicRuleEngineModel.getDescription())
                .priority(dynamicRuleEngineModel.getPriority())
                .when(new MVELCondition(dynamicRuleEngineModel.getConditionExpression()));
        if(StringUtils.isNotEmpty(dynamicRuleEngineModel.getActionExpression())){
            List<String> dataList = splitter.splitToList(dynamicRuleEngineModel.getActionExpression());
            for(String actionExpression:dataList){
                ruleBuilder.then(new MVELAction(actionExpression));
            }
        }
        return ruleBuilder.build();
    }

}
