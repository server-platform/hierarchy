package com.lhy.hierarchy.ruleEngine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 规则引擎信息
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("dynamic_rule_engine")
public class DynamicRuleEngineEntity extends Model<DynamicRuleEngineEntity> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 引擎名称
     */
    @TableField("name")
    private String name;

    /**
     * 描述信息
     */
    @TableField("description")
    private String description;

    /**
     * 告诉引擎规则被触发时跳过后面的规则
     */
    @TableField("skip_on_first_applied_rule")
    private Integer skipOnFirstAppliedRule;

    /**
     * 告诉引擎在规则失败时跳过后面的规则
     */
    @TableField("skip_on_first_failed_rule")
    private Integer skipOnFirstFailedRule;

    /**
     * 告诉引擎如果优先级超过定义的阈值
     */
    @TableField("rule_priority_threshold")
    private Integer rulePriorityThreshold;

    /**
     * 告诉引擎一个规则不会被触发跳过后面的规则
     */
    @TableField("skip_on_first_non_triggered_rule")
    private Integer skipOnFirstNonTriggeredRule;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 是否禁用
     */
    @TableField("is_disable")
    private Integer isDisable;

    /**
     * 删除状态
     */
    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
