/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy.hierarchy.ruleEngine.process;

import com.google.common.base.Splitter;
import com.lhy.hierarchy.ruleEngine.engine.DynamicRuleEngine;
import com.lhy.hierarchy.ruleEngine.enums.BooleanEnum;
import com.lhy.hierarchy.ruleEngine.enums.StatusEnum;
import com.lhy.hierarchy.ruleEngine.model.DynamicRuleEngineModel;
import com.lhy.hierarchy.ruleEngine.model.DynamicRuleModel;
import org.apache.commons.lang3.StringUtils;
import org.jeasy.rules.api.Rule;
import org.jeasy.rules.api.RulesEngine;
import org.jeasy.rules.api.RulesEngineParameters;
import org.jeasy.rules.core.DefaultRulesEngine;
import org.jeasy.rules.core.RuleBuilder;
import org.jeasy.rules.mvel.MVELAction;
import org.jeasy.rules.mvel.MVELCondition;

import java.util.List;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.process
 * @author:LiBo/Alex
 * @create-date:2022-08-13 22:37
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 *
 * 可以使用RulesEngineParameters API指定这些参数：
 *
 * RulesEngineParameters parameters = new RulesEngineParameters()
 *     .rulePriorityThreshold(10)
 *     .skipOnFirstAppliedRule(true)
 *     .skipOnFirstFailedRule(true)
 *     .skipOnFirstNonTriggeredRule(true);
 *
 * RulesEngine rulesEngine = new DefaultRulesEngine(parameters);
 *
 * 如果要从引擎获取参数，可以使用以下代码段：
 *
 * RulesEngineParameters parameters = myEngine.getParameters();
 *
 * 使用InferenceRulesEngine引擎，会通过循环不停的通过事实去匹配规则，直到全部事实都不匹配规则才停止循环。
 *
 * 可以看到源码中就是通过do while循环的 *
 *
 * 这允许您在创建引擎后重置引擎参数。
 *
 */
public class DynamicRuleEngineModelWrapper {

    /**
     * 阈值执行优先级值
     */
    private static final int DEFAULT_THRESHOLD_VALUE = 1000;

    /**
     * wrapper包装方法
     * @param dynamicRuleEngineModel
     * @return
     *
     * 要创建规则引擎，可以使用每个实现的构造函数：
     *
     * 根据规则的自然顺序（默认为优先级）应用规则。
     * RulesEngine rulesEngine = new DefaultRulesEngine();
     *
     * 持续对已知事实应用规则，直到不再应用规则为止。
     * RulesEngine rulesEngine = new InferenceRulesEngine();
     *
     * 然后，您可以按以下方式触发注册规则：
     *
     * rulesEngine.fire(rules, facts);
     */
    public RulesEngine wrapper(DynamicRuleEngineModel dynamicRuleEngineModel){

        /**
         *
         * 规则引擎参数
         *
         * Easy Rules 引擎可以配置以下参数：
         *
         * Parameter	                Type	    Required	Default
         * rulePriorityThreshold	    int	            no	    MaxInt
         * skipOnFirstAppliedRule	    boolean	        no	    false
         * skipOnFirstFailedRule	    boolean	        no	    false
         * skipOnFirstNonTriggeredRule	boolean	        no	    false
         */

        RulesEngineParameters parameters = new RulesEngineParameters();
        //告诉引擎如果优先级超过定义的阈值，则跳过下一个规则。版本3.3已经不支持更改，默认MaxInt。
        parameters.setPriorityThreshold(dynamicRuleEngineModel.getPriorityThreshold()<=0?
                DEFAULT_THRESHOLD_VALUE:dynamicRuleEngineModel.getPriorityThreshold());
        // 告诉引擎规则被触发时跳过后面的规则
        parameters.setSkipOnFirstAppliedRule(Boolean.
                parseBoolean(BooleanEnum.parse(dynamicRuleEngineModel.
                getSkipOnFirstAppliedRule(),BooleanEnum.FALSE).name()));
        // 告诉引擎在规则失败时跳过后面的规则
        parameters.setSkipOnFirstFailedRule(Boolean.parseBoolean(BooleanEnum.
                parse(dynamicRuleEngineModel.getSkipOnFirstFailedRule(),BooleanEnum.FALSE).name()));
        // 告诉引擎一个规则不会被触发跳过后面的规则
        parameters.setSkipOnFirstNonTriggeredRule(Boolean.parseBoolean(BooleanEnum.
                parse(dynamicRuleEngineModel.getSkipOnFirstNonTriggeredRule(),BooleanEnum.FALSE).name()));
        return new DefaultRulesEngine(parameters);
    }

}
