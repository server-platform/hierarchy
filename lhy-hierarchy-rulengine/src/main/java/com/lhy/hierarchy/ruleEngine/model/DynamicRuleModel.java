/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy.hierarchy.ruleEngine.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.model
 * @author:LiBo/Alex
 * @create-date:2022-08-14 00:08
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@NoArgsConstructor
@Accessors(chain = true)
@Data
public class DynamicRuleModel {

    /**
     * 规则名称
     */
    private String ruleName;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 顺序编号
     */
    private int priority;

    /**
     * 条件表达式
     */
    private String conditionExpression;

    /**
     * 行为表达式，采用逗号分割
     */
    private String actionExpression;
}
