package com.lhy.hierarchy.ruleEngine.composite.utils;

import java.util.Collection;

/**
 * Java规则类存储器
 * @author z_hh  
 * @date 2018年12月12日
 */
public interface JavaRuleStorage {
	
	/**
	 * 容器是否包含指定规则
	 * @param javaRule
	 * @return
	 */
	boolean contains(String groupName, BaseRule rule);

	/**
	 * 添加规则到容器
	 * @param javaRule
	 */
	boolean add(String groupName, BaseRule rule);
	
	/**
	 * 批量添加规则到容器的指定组
	 * @param javaRules
	 */
	boolean batchAdd(String groupName, Iterable<? extends BaseRule> rules);
	
	/**
	 * 从容器移除指定规则
	 * @param group
	 */
	boolean remove(String groupName, BaseRule rule);
	
	/**
	 * 从容器移除指定组的规则
	 * @param group
	 */
	boolean remove(String group);
	
	/**
	 * 从容器获取指定组的所有规则
	 * @param group
	 * @return
	 */
	Collection<BaseRule> listObjByGroup(String group);
}
