package com.lhy.hierarchy.ruleEngine.service;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleGroupEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 动态规则组信息 服务类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
public interface MPDynamicRuleGroupRepository extends IService<DynamicRuleGroupEntity> {

}
