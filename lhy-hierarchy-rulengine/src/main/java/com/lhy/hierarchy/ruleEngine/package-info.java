/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.ruleEngine.engine.namespace
 * @author:LiBo/Alex
 * @create-date:2022-08-14 13:18
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
package com.lhy.hierarchy.ruleEngine;

import org.jeasy.rules.api.Facts;

/**
 * 名称：规则命名空间中的唯一规则名称
 *
 * 定义规则
 *
 * 大多数业务规则可以由以下定义表示：
 *
 * 名称：规则命名空间中的唯一规则名称
 * 说明：规则的简要说明优先级：相对于其他规则的规则优先级
 * 事实：去匹配规则时的一组已知事实
 * 条件：为了匹配该规则，在给定某些事实的情况下应满足的一组条件
 * 动作：当条件满足时要执行的一组动作（可以添加/删除/修改事实）
 *
 * Easy Rules为定义业务规则的每个关键点提供了抽象。
 *
 * 在Easy Rules中，一个规则由Rule接口表示：
 *
 * public interface Rule {
 *
 *
 *     改方法封装规则的条件（conditions）
 *     @return 如果提供的事实适用于该规则返回true, 否则，返回false
 *     evaluate方法封装了必须求值为TRUE才能触发规则的条件
 *     boolean evaluate(Facts facts);
 *
 *
 *     改方法封装规则的操作（actions）
 *     execute方法封装了在满足规则条件时应执行的操作。条件和动作ConditionandAction接口表示。
 *     @throws 如果在执行过程中发生错误将抛出Exception
 *
 *     void execute(Facts facts) throws Exception;
 *
 *     //Getters and setters for rule name, description and priority omitted.
 *
 * }
 *
 *
 *
 *
 *
 */


