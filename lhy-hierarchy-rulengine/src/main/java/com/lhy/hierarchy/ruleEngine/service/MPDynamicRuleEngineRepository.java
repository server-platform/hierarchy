package com.lhy.hierarchy.ruleEngine.service;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleEngineEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 规则引擎信息 服务类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
public interface MPDynamicRuleEngineRepository extends IService<DynamicRuleEngineEntity> {

}
