package com.lhy.hierarchy.ruleEngine.service.impl;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleEngineEntity;
import com.lhy.hierarchy.ruleEngine.mapper.IDynamicRuleEngineDao;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.hierarchy.ruleEngine.service.MPDynamicRuleEngineRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 规则引擎信息 服务实现类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Service
public class DynamicRuleEngineRepositoryImpl extends ServiceImpl<IDynamicRuleEngineDao, DynamicRuleEngineEntity> implements MPDynamicRuleEngineRepository {

}
