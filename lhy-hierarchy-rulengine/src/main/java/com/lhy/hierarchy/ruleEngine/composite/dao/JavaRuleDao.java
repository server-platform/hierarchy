package com.lhy.hierarchy.ruleEngine.composite.dao;

import com.hauxsoft.core.mvc.dao.BaseDao;
import com.hauxsoft.entity.JavaRuleDO;

/**
 *
 * Hauxsoft Code Generate @ 2018-12-05 10:14:09
 * Copyright © 2018 Haux Soft Incorporated. All rights reserved. 
 *
 */
public interface JavaRuleDao extends BaseDao<JavaRuleDO, Long> {

}