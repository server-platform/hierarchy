package com.lhy.hierarchy.ruleEngine.service.impl;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleConfigEntity;
import com.lhy.hierarchy.ruleEngine.mapper.IDynamicRuleConfigDao;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.hierarchy.ruleEngine.service.MPDynamicRuleConfigRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 动态规则信息 服务实现类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Service
public class DynamicRuleConfigRepositoryImpl extends ServiceImpl<IDynamicRuleConfigDao, DynamicRuleConfigEntity> implements MPDynamicRuleConfigRepository {

}
