package com.lhy.hierarchy.ruleEngine.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 动态规则信息
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("dynamic_rule_config")
public class DynamicRuleConfigEntity extends Model<DynamicRuleConfigEntity> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 分组id
     */
    @TableField("rule_group_id")
    private Long ruleGroupId;

    /**
     * 规则引擎id
     */
    @TableField("rule_engine_id")
    private Long ruleEngineId;

    /**
     * 复杂的规则类型-1 简单类型-0
     */
    @TableField("rule_type")
    private Integer ruleType;

    /**
     * 规则名称
     */
    @TableField("name")
    private String name;

    /**
     * 描述信息
     */
    @TableField("description")
    private String description;

    /**
     * 优先级执行
     */
    @TableField("priority")
    private Integer priority;

    /**
     * 条件判断
     */
    @TableField("condition")
    private String condition;

    /**
     * 行为列表操作
     */
    @TableField("actions")
    private String actions;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 是否禁用
     */
    @TableField("is_disable")
    private Integer isDisable;

    /**
     * 删除状态
     */
    @TableField("is_delete")
    private Integer isDelete;


    @Override
    public Serializable pkVal() {
        return this.id;
    }

}
