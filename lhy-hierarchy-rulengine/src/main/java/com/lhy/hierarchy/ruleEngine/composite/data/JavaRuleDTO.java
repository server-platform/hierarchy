package com.lhy.hierarchy.ruleEngine.composite.data;

import com.hauxsoft.core.constant.Consts;
import com.hauxsoft.core.util.BeanUtils2;
import com.hauxsoft.entity.JavaRuleDO;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * Hauxsoft Code Generate @ 2018-12-05 10:14:09
 * Copyright © 2018 Haux Soft Incorporated. All rights reserved. 
 *
 */
@Getter
@Setter
@ToString
public class JavaRuleDTO implements Serializable{
    private static final long serialVersionUID = -8863050348399536433L;
    
    private Long id;//主键
    private String target;//目标
    private String fileName;//文件名
    private String fullClassName;//全类名
    private String simpleClassName;//类名
    private String srcCode;//源码
    private Date createTime;//创建时间
    private Long createUserId = Consts.Entity.NULL_ID_PLACEHOLDER;//创建用户id
    private String createUserName;//创建用户名称
    private Date updateTime;//更新时间
    private Long updateUserId = Consts.Entity.NULL_ID_PLACEHOLDER;//更新用户id
    private String updateUserName;//更新用户名称
    private Integer status = Consts.Entity.IS_VALID;//状态，1有效 0无效
 	private String groupName;//组别名称，一般指哪一系列规则
 	private Integer sort;//顺序（优先级）
 	private String name;//规则名称
 	private String description;//规则描述
 	
    public JavaRuleDO toEntity(String... ignoreProperties) {
        JavaRuleDO entity = new JavaRuleDO();
        BeanUtils2.copyProperties(this, entity, ignoreProperties);
        return entity;
    }
    
    public static JavaRuleDTO of(JavaRuleDO entity, String... ignoreProperties) {
        JavaRuleDTO dto = BeanUtils2.createWithProperties(entity, JavaRuleDTO.class, ignoreProperties);
        return dto;
    }
}