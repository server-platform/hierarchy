package com.lhy.hierarchy.ruleEngine.service;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleRecordEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 动态规则记录 服务类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
public interface MPDynamicRuleRecordRepository extends IService<DynamicRuleRecordEntity> {

}
