package com.lhy.hierarchy.ruleEngine.composite.conf;

import com.hauxsoft.utils.JavaRuleStorage;
import com.hauxsoft.utils.MapJavaRuleStorage;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RuleDefaultConf {

	@Bean
	@ConditionalOnMissingBean
	public JavaRuleStorage javaRuleStorage() {
		return new MapJavaRuleStorage();
	}
}
