package com.lhy.hierarchy.ruleEngine.composite.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.hauxsoft.core.constant.Consts;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * Hauxsoft Code Generate @ 2018-12-05 10:14:09 Copyright © 2018 Haux Soft
 * Incorporated. All rights reserved.
 *
 */
@Getter
@Setter
@ToString
@Entity
@Table(name = "JAVA_RULE")
@JsonIgnoreProperties(value = { "hibernateLazyInitializer", "handler", "fieldHandler", "historyArcticlesLinkMapping" })
public class JavaRuleDO implements Serializable {
	private static final long serialVersionUID = 830103606495004702L;

	@Id
	@GeneratedValue(generator = "JavaRuleIdGenerator")
	@GenericGenerator(name = "JavaRuleIdGenerator", strategy = "com.hauxsoft.core.mvc.dao.IdGenerator", parameters = {
			@Parameter(name = "sequence", value = "SEQ_JAVA_RULE") })
	private Long id;
	// 目标，一般指哪个系统
	@Column
	private String target;
	// 文件名
	@Column
	private String fileName;
	// 全类名
	@Column
	private String fullClassName;
	// 类名
	@Column
	private String simpleClassName;
	// 源码
	@Column
	private String srcCode;
	// 编译后字节码
	@Column
	private byte[] byteContent;
	// 创建时间
	@Column
	private Date createTime;
	// 创建用户id
	@Column
	private Long createUserId = Consts.Entity.NULL_ID_PLACEHOLDER;
	// 创建用户名称
	@Column
	private String createUserName;
	// 更新时间
	@Column
	private Date updateTime;
	// 更新用户id
	@Column
	private Long updateUserId = Consts.Entity.NULL_ID_PLACEHOLDER;
	// 更新用户名称
	@Column
	private String updateUserName;
	// 是否已删除，1是 0否
	@Column
	private Integer isDeleted = Consts.Entity.NOT_DELETED;
	// 状态，1有效 0无效
	@Column
	private Integer status = Consts.Entity.NOT_VALID;
	// 组别名称，一般指哪一系列规则
	@Column
	private String groupName;
	// 顺序（优先级）
	@Column
	private Integer sort = Integer.MAX_VALUE;
	// 规则名称
	@Column
	private String name;
	// 规则描述
	@Column
	private String description;
	
}