package com.lhy.hierarchy.ruleEngine.mapper;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleGroupEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 动态规则组信息 Mapper 接口
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Mapper
public interface IDynamicRuleGroupDao extends BaseMapper<DynamicRuleGroupEntity> {

}
