package com.lhy.hierarchy.ruleEngine.service.impl;

import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleGroupEntity;
import com.lhy.hierarchy.ruleEngine.mapper.IDynamicRuleGroupDao;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lhy.hierarchy.ruleEngine.service.MPDynamicRuleGroupRepository;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 动态规则组信息 服务实现类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
@Service
public class DynamicRuleGroupRepositoryImpl extends ServiceImpl<IDynamicRuleGroupDao, DynamicRuleGroupEntity> implements MPDynamicRuleGroupRepository {

}
