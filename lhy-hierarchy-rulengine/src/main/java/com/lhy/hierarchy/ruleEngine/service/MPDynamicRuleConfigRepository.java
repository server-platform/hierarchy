package com.lhy.hierarchy.ruleEngine.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.lhy.hierarchy.ruleEngine.entity.DynamicRuleConfigEntity;

/**
 * <p>
 * 动态规则信息 服务类
 * </p>
 *
 * @author libo
 * @since 2022-08-13
 */
public interface MPDynamicRuleConfigRepository extends IService<DynamicRuleConfigEntity> {

}
