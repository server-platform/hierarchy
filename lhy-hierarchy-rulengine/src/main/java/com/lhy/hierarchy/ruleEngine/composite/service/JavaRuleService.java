package com.lhy.hierarchy.ruleEngine.composite.service;

import com.hauxsoft.core.mvc.service.BaseFlatCrudService;
import com.hauxsoft.core.util.result.Result;
import com.hauxsoft.data.JavaRuleDTO;
import com.hauxsoft.entity.JavaRuleDO;

public interface JavaRuleService extends BaseFlatCrudService<JavaRuleDO, Long>{

    /**
     * 返回dto对象 
     * @param id
     * @return success result if entity exist, or error result if entity not exist
     */
    Result<JavaRuleDTO> getDTO(Long id);

    /**
     * 新增一条记录
     * @param dto
     * @return success or error result
     */
    Result<?> insertDTO(JavaRuleDTO dto);
    
    /**
     * 修改一条记录
     * @param dto
     * @return success or error result
     */
    Result<?> updateDTO(JavaRuleDTO dto);
    
    /**
     * 返回entity对象，如果entity不存在，或者被逻辑删除，则返回error result对象
     * @param id
     * @return success or error result
     */
    Result<JavaRuleDO> getOne(Long id);
   
    /**
     * 返回一条有效的entity对象，如果entity不存在，或者被逻辑删除，或者状态不在有效范围，则返回error result对象
     * @param id
     * @return
     */
    Result<JavaRuleDO> getValidOne(Long id);
 
     /**
     * enable, set entity.status from 0 -> 1
     * @param id
     * @return
     */
    Result<JavaRuleDO> enable(Long id);
    
    /**
     * enable, set entity.status from 0 -> 1
     * @param id
     * @return
     */
    Result<JavaRuleDO> enable(JavaRuleDO entity);
    
    /**
     * disable, set entity.status from 1 -> 0
     * @param id
     * @return
     */
    Result<JavaRuleDO> disable(Long id);
    
    /**
     * disable, set entity.status from 1 -> 0
     * @param id
     * @return
     */
    Result<JavaRuleDO> disable(JavaRuleDO entity);
    
    /**
     * submit, set entity.status from -1 -> 0
     * @param id
     * @return
     */
    Result<JavaRuleDO> submit(Long id);
    
    /**
     * submit, set entity.status from -1 -> 0
     * @param id
     * @return
     */
    Result<JavaRuleDO> submit(JavaRuleDO entity);
    
    /**
     * withdraw, set entity.status from 0 -> -1
     * @param id
     * @return
     */
    Result<JavaRuleDO> withdraw(Long id);
    
    /**
     * withdraw, set entity.status from 0 -> -1
     * @param id
     * @return
     */
    Result<JavaRuleDO> withdraw(JavaRuleDO entity);
    
    /**
     * recover, set entity.isDeleted from 1 -> 0
     * @param id
     * @return
     */
    Result<JavaRuleDO> recover(Long id);
    
    /**
     * recover, set entity.isDeleted from 1 -> 0
     * @param id
     * @return
     */
    Result<JavaRuleDO> recover(JavaRuleDO entity);
    
    /**
     * 返回entity对象，如果entity不存在，或者被逻辑删除，则返回error result对象
     * @param id
     * @return success or error result
     */
    Result<?> delete(JavaRuleDO entity);

}