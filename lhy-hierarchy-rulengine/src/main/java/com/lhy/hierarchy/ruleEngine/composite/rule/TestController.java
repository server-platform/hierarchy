package com.lhy.hierarchy.ruleEngine.composite.rule;

import com.hauxsoft.component.DynamicRuleManager;
import com.hauxsoft.component.DynamicRuleManager.Builder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

	@GetMapping("/rule/test")
	public void test() {
		Builder builder = dynamicRuleManager.builder()
			.setParameter("param1", "Hello")
			.setParameter("param2", "World")
			.addRuleGroup("testRule")
			.run();
		String param1 = builder.getParameter("param1", String.class);
		String param2 = builder.getParameter("param2", String.class);
		System.out.println(param1 + " " + param2);
	}
	
	@Autowired
	private DynamicRuleManager dynamicRuleManager;
}
