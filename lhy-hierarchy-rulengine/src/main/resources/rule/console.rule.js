/**
 *
 * Hauxsoft Code Generate @ 2018-12-05 10:14:09
 * Copyright © 2018 Haux Soft Incorporated. All rights reserved. 
 *
 */
angular.module("h.console.rule", [])

//1.declare router constant，don't regist router path in ui.router
.constant("CONSOLE_RULE_ROUTER", [
    {
        state : "app.java-rule-list", 
        url : "/api/console/java/rules?pageNo&pageSize&condition", 
        templateUrl : "/console/module/rule/java-rule-list.html"
    },
    {
        state : "app.java-rule-edit", 
        url : "/api/console/java/rules/{id:int}", 
        templateUrl : "/console/module/rule/java-rule-edit.html"
    },
    {
        state : "app.java-rule-add", 
        url : "/api/console/java/rules/add?copyId", 
        templateUrl : "/console/module/rule/java-rule-edit.html"
    },
    {
        state : "app.java-rule-view", 
        url : "/api/console/java/rules/{id:int}/view", 
        templateUrl : "/console/module/rule/java-rule-view.html"
    }     
])

/*.constant("CONSOLE_PROTOTYPE_MENU_ITEMS", [
    {"title":"动态规则", "target":"app.java-rule-list"},
])*/

.factory("JavaRuleService", ["$crud", function($crud){
    return $crud("/api/console/java/rules");
}])

.controller("javaRuleListController", ["$scope", "$hui", "$injector", "$timeout", "$util", "$option",
function($scope, $hui, $injector, $timeout, $util, $option){
    var service = $injector.get("JavaRuleService");
    $util.controller.call("$listController", "$scope", $scope, "service", service, "config", {});
    
    $option.bind($scope, "javaRule", "isDeleted", "status");
}])

.controller("javaRuleEditController", ["$scope", "$hui", "$injector", "$q", "$util", "$option",
function($scope, $hui, $injector, $q, $util, $option){
    var service = $injector.get("JavaRuleService");
    $util.controller.call("$editController", "$scope", $scope, "service", service, "config", {});
    $option.bind($scope, "javaRule", "isDeleted", "status");
}])

.run(["$option", function($option){
    $option.builder("javaRule", "value", "text")
        .option(1, "已删除").option(0, "未删除").put("isDeleted")//是否已删除，1是 0否
        .option(1, "启用").option(0, "停用").put("status");//状态，1启用 0禁用

}])