package com.lhy.delayqueue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
@EnableAsync
public class DelayedQueueApplication {
	public static void main(String[] args) {
		System.setProperty("spring.main.allow-circular-references","true");
		SpringApplication.run(DelayedQueueApplication.class);
	}
}
