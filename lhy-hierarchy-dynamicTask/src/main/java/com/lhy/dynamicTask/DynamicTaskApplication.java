package com.lhy.dynamicTask;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("com.lhy.dynamicTask.dao")
@SpringBootApplication
public class DynamicTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(DynamicTaskApplication.class, args);
    }
}
