package com.lhy.hierarchy.emailsend.model;


import com.lhy.hierarchy.emailsend.enums.OfficalSmtpEnum;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * 邮件配置项 封装类
 */
@Data
@Accessors(chain = true)
public class MailSenderConfig {

    /**
     * 发件人邮箱
     */
    private String username;
    /**
     * 发件人邮箱密码（qq邮箱、163邮箱需要的是邮箱授权码，新浪邮箱直接是邮箱登录密码）
     */
    private String password;
    /**
     * 发件人定制别名。
     */
    private String senderNickname;
    /**
     * 支持的邮箱Host。{@link OfficalSmtpEnum}
     */
    private OfficalSmtpEnum mailSmtpHost;
    /**
     * 是否开启debug。默认false
     */
    private Boolean mailDebug = Boolean.FALSE;
    /**
     * 自定义 MiniEmail实现类路径。类必须继承 BaseMiniEmail
     */
    private String customMiniEmail;
    /**
     * 开启账号密码鉴权。默认true
     */
    private Boolean mailSmtpAuth = Boolean.TRUE;
    /**
     * 开启SSL。默认true
     */
    private Boolean mailSmtpSslEnable = Boolean.TRUE;
    /**
     * 传输协议。默认smtp
     */
    private String mailTransportProtocol = "smtp";
    /**
     * 超时时间。默认10s
     */
    private Long mailSmtpTimeout = 10000L;
    /**
     * 端口。默认465
     */
    private Integer mailSmtpPort = 465;

    /**
     * 配置服务控制
     * @param username
     * @param password
     * @return
     */
    public static MailSenderConfig config(String username, String password) {
        return new MailSenderConfig().setUsername(username).setPassword(password);
    }

}
