package com.lhy.hierarchy.emailsend.factory;

import cn.hutool.cron.CronUtil;
import cn.hutool.cron.task.Task;
import com.lhy.hierarchy.emailsend.builder.EmailSessionBuilder;
import com.lhy.hierarchy.emailsend.engine.AbstractEmailSender;
import com.lhy.hierarchy.emailsend.engine.ProcessTemplateEmailSender;
import com.lhy.hierarchy.emailsend.engine.EmailSender;
import com.lhy.hierarchy.emailsend.model.MailSenderConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;

/**
 * 默认 MiniEmailFactory
 *
 * @author thundzeng
 */
@Slf4j
public class DefaultEmailSendFactory implements EmailSendFactory {


    private EmailSessionBuilder sessionBuilder;

    private String finalSenderName;

    private String customMiniEmailPath;

    /**
     * 构造器
     * @param config
     */
    public DefaultEmailSendFactory(MailSenderConfig config) {
        // init session
        this.sessionBuilder = new EmailSessionBuilder(config);
        this.finalSenderName = constructFinalSenderName(config.getSenderNickname(), config.getUsername());
        this.customMiniEmailPath = config.getCustomMiniEmail();
    }


    /**
     * 发件人昵称 构建
     * @param senderNickname 自定义昵称
     * @param username       发件人邮箱
     * @return String 处理后的发件人昵称
     */
    protected String constructFinalSenderName(String senderNickname, String username) {
        boolean notDealName = StringUtils.isEmpty(senderNickname) || senderNickname.equals(username);
        if (notDealName) {
            return username;
        }

        // 如果有定制发件人昵称，此处作处理
        try {
            senderNickname = MimeUtility.encodeText(senderNickname) + " <" + username + ">";
        } catch (UnsupportedEncodingException e) {
            log.warn("## senderNickname 解析失败，重新使用 username 作为发件人别名。");
        }

        // 若定制发件人定制昵称为空，则使用 username 作为发件人别名
        return StringUtils.isEmpty(senderNickname) ? username : senderNickname;
    }

    /**
     * init 初始化
     * @param clazz
     * @return
     */
    protected EmailSender init(Class<? extends AbstractEmailSender> clazz) {
        AbstractEmailSender baseMiniEmail = null;
        try {
            baseMiniEmail = clazz.getDeclaredConstructor(MimeMessage.class, String.class)
                    .newInstance(new MimeMessage(sessionBuilder.parseSession()), finalSenderName);
        } catch (InstantiationException | NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            log.error("init the email sender is failure!",e);
        }
        if (null == baseMiniEmail) {
            throw new RuntimeException("初始化 MiniEmail 失败，请检查类路径正确以及是否继承 BaseMiniEmail。");
        }

        return baseMiniEmail;
    }

    /**
     * 创建操作
     * @return
     */
    @Override
    public EmailSender create() {
        if (StringUtils.isEmpty(customMiniEmailPath)) {
            return this.init(ProcessTemplateEmailSender.class);
        }
        // 加载自定义实现类
        Class<AbstractEmailSender> customClass = null;
        try {
            customClass = (Class<AbstractEmailSender>) ProcessTemplateEmailSender.class
                    .getClassLoader()
                    .loadClass(customMiniEmailPath);
        } catch (ClassNotFoundException e) {
            log.error("create the EmailSender is failure!",e);
        }
        return null == customClass ? null : this.init(customClass);
    }


   
}
