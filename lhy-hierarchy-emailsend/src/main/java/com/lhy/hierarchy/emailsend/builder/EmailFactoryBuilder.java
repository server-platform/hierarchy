package com.lhy.hierarchy.emailsend.builder;

import com.lhy.hierarchy.emailsend.enums.OfficalSmtpEnum;
import com.lhy.hierarchy.emailsend.errors.ParameterException;
import com.lhy.hierarchy.emailsend.factory.DefaultEmailSendFactory;
import com.lhy.hierarchy.emailsend.factory.EmailSendFactory;
import com.lhy.hierarchy.emailsend.model.MailSenderConfig;
import org.apache.commons.lang3.StringUtils;

/**
 * 建造 {@link com.lhy.hierarchy.emailsend.engine.EmailSender} 实例。
 * @author thundzeng
 */
public class EmailFactoryBuilder {

	/**
	 * 建造 MiniEmailFactory
	 *
	 * @param debug    是否开启debug。
	 * @param username 发件人邮箱。
	 * @param password 发件人邮箱密码（qq邮箱、163邮箱需要的是邮箱授权码，新浪邮箱直接是邮箱登录密码）。
	 * @param smtpEnum 支持的邮箱Host。{@link OfficalSmtpEnum}
	 * @return MiniEmailFactory
	 */
	public EmailSendFactory build(boolean debug, String username, String password, OfficalSmtpEnum smtpEnum) {
		MailSenderConfig config = MailSenderConfig.config(username, password)
				.setMailSmtpHost(smtpEnum)
				.setMailDebug(debug);

		return this.build(config);
	}

	/**
	 * 建造 MiniEmailFactory
	 *
	 * @param config 配置参数。
	 * @return MiniEmailFactory
	 */
	public EmailSendFactory build(MailSenderConfig config) {
		this.checkParameter(config);

		return new DefaultEmailSendFactory(config);
	}

	/**
	 * 参数检查
	 * @param config 配置参数。
	 */
	private void checkParameter(MailSenderConfig config) {
		if (StringUtils.isEmpty(config.getUsername()) || StringUtils.isEmpty(config.getPassword())) {
			throw new ParameterException("请填写完整的收件人信息");
		}
		if (null == config.getMailSmtpHost()) {
			throw new ParameterException("请选择邮件Host");
		}
	}
}
