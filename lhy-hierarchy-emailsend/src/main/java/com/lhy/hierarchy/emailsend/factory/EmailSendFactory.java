package com.lhy.hierarchy.emailsend.factory;

import com.lhy.hierarchy.emailsend.engine.EmailSender;

/**
 * 邮件工厂接口
 */
public interface EmailSendFactory {

    /**
     * 初始化邮件信息简易版
     * @return MiniEmail
     * @since 1.3.0
     */
    EmailSender create();

}
