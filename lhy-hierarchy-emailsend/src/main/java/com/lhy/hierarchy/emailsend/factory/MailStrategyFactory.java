package com.lhy.hierarchy.emailsend.factory;

import com.lhy.hierarchy.emailsend.enums.EmailContentTypeEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 邮件发送bean工厂类
 *
 * @author TongWei.Chen 2018-06-15 16:39:07
 * @Project common-boot-email
 */
public class MailStrategyFactory {

    private MailStrategyFactory() {}

    private static final class InnerMailFactory {
        private static final MailStrategyFactory MAIL_FACTORY = new MailStrategyFactory();
    }

    private static Map<EmailContentTypeEnum, String> maps = new HashMap<>();

    static {
        maps.put(EmailContentTypeEnum.TEXT, getStrategyClassName("simple"));
        maps.put(EmailContentTypeEnum.HTML, getStrategyClassName("html"));
        maps.put(EmailContentTypeEnum.TEMPLATE, getStrategyClassName("template"));
    }

    public static final MailStrategyFactory getInstance() {
        return InnerMailFactory.MAIL_FACTORY;
    }



    public String get(String type) {
        return maps.get(type);
    }

    /**
     * 获取策略类名
     *
     * @param classNamePrefix：类名前缀
     * @return
     */
    private static String getStrategyClassName(String classNamePrefix) {
        return classNamePrefix + "MailStrategy";
    }

}
