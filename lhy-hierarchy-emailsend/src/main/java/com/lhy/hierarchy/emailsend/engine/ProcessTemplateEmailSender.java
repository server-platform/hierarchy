/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy.hierarchy.emailsend.engine;

import cn.hutool.core.map.MapUtil;
import com.lhy.hierarchy.emailsend.enums.EmailContentTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy.hierarchy.emailsend.core
 * @author:LiBo/Alex
 * @create-date:2022-08-13 12:43
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 拥有处理模板控制邮件发送功能机制
 */

@Slf4j
@Component
public class ProcessTemplateEmailSender extends AbstractEmailSender{


    /**
     * 模板引擎
     */
    @Autowired
    private TemplateEngine templateEngine;

    /**
     * 构造器
     * @param msg
     * @param fromName
     */
    public ProcessTemplateEmailSender(MimeMessage msg, String fromName) {
        super(msg, fromName);
    }

    /**
     *
     * @param to          接收邮箱
     * @param subject     邮件主题
     * @param contentType 邮件内容格式类型
     * @param content     发送内容
     */
    @Override
    public void send(String to, String subject, EmailContentTypeEnum contentType, String content) {
        super.send(to, subject, contentType, content);
    }


    /**
     * 发送消息
     * @param to 接收邮箱
     * @param subject 邮件主题
     * @param contentType 邮件内容格式类型
     * @param templateContent 发送内容
     * @param variableParameter 参数信息
     */
    @Override
    public void send(String to, String subject, EmailContentTypeEnum contentType, String templateContent,
                     Map<String, Object> variableParameter) {
        send(new String[]{to}, subject, contentType, templateContent, variableParameter);
    }


    /**
     * 发送消息
     * @param tos 接收邮箱
     * @param subject 邮件主题
     * @param contentType 邮件内容格式类型
     * @param templateContent 发送内容
     * @param variableParameter 参数信息
     */
    @Override
    public void send(String[] tos, String subject, EmailContentTypeEnum contentType,
                     String templateContent,
                     Map<String, Object> variableParameter) {
        try {
            final Context ctx = new Context(new Locale(""));
            if (MapUtil.isNotEmpty(variableParameter)) {
                for (Map.Entry<String, Object> entry : variableParameter.entrySet()) {
                    ctx.setVariable(entry.getKey(), entry.getValue());
                }
            }
            final String htmlContent = templateEngine.process(templateContent, ctx);
            config(subject, tos, contentType, htmlContent);
            msg.setSentDate(Calendar.getInstance().getTime());
            Transport.send(msg);
        } catch (MessagingException e) {
            log.error("send the message is failure!",e);
        } finally {
            try {
                clearContentAfterSend();
            } catch (MessagingException e) {
                log.error("send the message is failure!",e);
            }
        }
    }
}
