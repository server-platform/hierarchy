/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.lhy;

import org.apache.commons.mail.*;
import org.apache.commons.mail.resolver.DataSourceUrlResolver;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * @project-name:lhy-hierarchy
 * @package-name:com.lhy
 * @author:LiBo/Alex
 * @create-date:2022-08-13 13:48
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class CommonEMailTest {

    /**
     * 发送内容为简单文本的邮件
     */
    @Test
    public void testSimpleEmail() throws Exception {
        // 发送简单的email,不能添加附件
        SimpleEmail email = new SimpleEmail();
        //QQ使用的是smtp服务器，需要身份验证
        // 邮件服务器域名
        email.setHostName("smtp.qq.com");
        // 邮件服务器smtp协议的SSL端口
        //POP3服务器（端口995）
        //SMTP服务器（端口465或587）
        email.setSmtpPort(465);
        //验证信息(发送的邮箱地址与密码) 注:这里的密码是授权码
        email.setAuthenticator(new DefaultAuthenticator("369950806@qq.com", "XXXXXXXXXXXXXXXX"));
        // 是否启用SSL
        email.setSSLOnConnect(true);
        // 设置字符编码方式
        email.setCharset("UTF-8");
        // 发件人
        email.setFrom("369950806@qq.com");
        // 收件人
        email.addTo("369950806@qq.com");
        //发送给多人
//     email.addTo(String... emails);
        // 抄送
//     email.addCc("xxx");
        // 密送
//     email.addBcc("xxx");
        //邮件的标题
        email.setSubject("第一封简单邮件");
        //邮件的内容
        email.setMsg("简单的邮件来了哦！！！");
        //发送
        email.send();
    }

    /**
     * 发送包含附件的邮件（附件为本地资源）
     */
    @Test
    public void sendEmailsWithAttachments() throws EmailException {
        // 附件类
        EmailAttachment attachment = new EmailAttachment();
        // 本地路径
        attachment.setPath("E:\\图片\\cxy.jpg");
        // 定义附件
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        // 附件描述
        attachment.setDescription("kind man");
        // 附件名（为中文时要处理编码）
        attachment.setName("hecai.jpg");

        //创建email对象(MultiPartEmail可以操作附件)
        MultiPartEmail email = new MultiPartEmail();

        // 邮件服务器域名
        email.setHostName("smtp.qq.com");
        // 邮件服务器smtp协议的SSL端口
        email.setSmtpPort(465);
        //验证信息(发送的邮箱地址与密码) 注:这里的密码是授权码
        email.setAuthenticator(new DefaultAuthenticator("369950806@qq.com", "XXXXXXXXXXXXXXXX"));
        // 是否启用SSL
        email.setSSLOnConnect(true);
        // 设置字符编码方式
        email.setCharset("UTF-8");

        email.setFrom("369950806@qq.com");
        email.addTo("369950806@qq.com");

        email.setSubject("这是一张图片");
        email.setMsg("我发了一张图片给你");
        email.attach(attachment);

        email.send();
    }

    /**
     * 发送包含附件的邮件（附件为在线资源）
     * 在发送时自动将网络上资源下载发送
     */
    @Test
    public void sendEmailsWithOnlineAttachments() throws EmailException, MalformedURLException {
        EmailAttachment attachment = new EmailAttachment();
        attachment.setURL(new URL("https://profile-avatar.csdnimg.cn/46cf5380bece4b3580eab60bca10a784_lianghecai52171314.jpg"));
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("csdn hc");
        attachment.setName("hecai.jpg");

        MultiPartEmail email = new MultiPartEmail();
        // 邮件服务器域名
        email.setHostName("smtp.qq.com");
        // 邮件服务器smtp协议的SSL端口
        email.setSmtpPort(465);
        //验证信息(发送的邮箱地址与密码) 注:这里的密码是授权码
        email.setAuthenticator(new DefaultAuthenticator("369950806@qq.com", "XXXXXXXXXXXXXXXX"));
        // 是否启用SSL
        email.setSSLOnConnect(true);
        // 设置字符编码方式
        email.setCharset("UTF-8");

        email.setFrom("369950806@qq.com");
        email.addTo("369950806@qq.com");

        email.setSubject("这是一张图片");
        email.setMsg("我发了一张图片给你");
        email.attach(attachment);

        email.send();
    }

    /**
     * 发送内容为HTML格式的邮件,内嵌图片
     */
    @Test
    public void sendHTMLFormattedEmail() throws EmailException, MalformedURLException {
        // HTML格式邮件，同时具有MultiPartEmail类所有“功能”
        HtmlEmail email = new HtmlEmail();

        // 邮件服务器域名
        email.setHostName("smtp.qq.com");
        // 邮件服务器smtp协议的SSL端口
        email.setSmtpPort(465);
        //验证信息(发送的邮箱地址与密码) 注:这里的密码是授权码
        email.setAuthenticator(new DefaultAuthenticator("369950806@qq.com", "XXXXXXXXXXXXXXXX"));
        // 是否启用SSL
        email.setSSLOnConnect(true);
        // 设置字符编码方式
        email.setCharset("UTF-8");

        email.setFrom("369950806@qq.com");
        email.addTo("369950806@qq.com");

        email.setSubject("测试邮件");

        // 图片的网络地址
        URL url = new URL("https://profile-avatar.csdnimg.cn/46cf5380bece4b3580eab60bca10a784_lianghecai52171314.jpg");
        String cid = email.embed(url, "云亮");

        // 将图片引入html标签
        email.setHtmlMsg("<html>梁云亮 <br/><img src='cid:" + cid + "'></html>");

        email.send();
    }

    /**
     * 发送内容为HTML格式的邮件,内嵌图片
     */
    @Test
    public void sendHTMLFormattedEmailWithEmbeddedImages() throws MalformedURLException, EmailException {
        // ImageHtmlEmail类通常是用来发送Html格式并内嵌图片的邮件，它拥有所有HtmlEmail的功能，但是图片主要是以html内嵌的为主
        ImageHtmlEmail email = new ImageHtmlEmail();

        // 邮件服务器域名
        email.setHostName("smtp.qq.com");
        // 邮件服务器smtp协议的SSL端口
        email.setSmtpPort(465);
        //验证信息(发送的邮箱地址与密码) 注:这里的密码是授权码
        email.setAuthenticator(new DefaultAuthenticator("369950806@qq.com", "XXXXXXXXXXXXXXXX"));
        // 是否启用SSL
        email.setSSLOnConnect(true);
        // 设置字符编码方式
        email.setCharset("UTF-8");

        email.setFrom("369950806@qq.com");
        email.addTo("369950806@qq.com");

        email.setSubject("测试邮件");


        URL url = new URL("https://hcshow.blog.csdn.net/");
        // 这样HTML内容里如果有此路径下的图片会直接内联
        email.setDataSourceResolver(new DataSourceUrlResolver(url));
        String htmlEmail = "这里<img src='https://profile-avatar.csdnimg.cn/46cf5380bece4b3580eab60bca10a784_lianghecai52171314.jpg'>一个认真负责任的人！";
        email.setHtmlMsg(htmlEmail);

        email.send();
    }
}
