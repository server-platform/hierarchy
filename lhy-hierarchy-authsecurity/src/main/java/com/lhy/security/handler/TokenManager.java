package com.lhy.security.handler;

import com.alibaba.fastjson.JSON;
import com.hyts.delayer.security.config.TokenProperties;
import com.hyts.delayer.security.entity.SecurityUser;
import io.jsonwebtoken.CompressionCodecs;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <p>
 * token管理
 * </p>
 *
 * @author qy
 * @since 2019-11-08
 */
@Component
public class TokenManager {



    private long tokenExpiration = 24*60*60*1000;


    private String tokenSignKey = "123456";

    @Autowired
    private TokenProperties tokenProperties;


    /**
     * 创建token对象数据
     * @param username
     * @return
     */
    public String createToken(String username) {
        String token = Jwts.builder().setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + tokenExpiration))
                .signWith(SignatureAlgorithm.HS512, tokenSignKey)
                .compressWith(CompressionCodecs.GZIP).compact();
        return token;
    }

    /**
     * 根据token获取相关的用户
     * @param token
     * @return
     */
    public String getUserFromToken(String token) {
        String user = Jwts.parser().setSigningKey(tokenSignKey).parseClaimsJws(token).getBody().getSubject();
        return user;
    }

    /**
     * 移除相关的token信息数据
     * @param token
     */
    public void removeToken(String token) {
        //jwttoken无需删除，客户端扔掉即可。
    }

    /**
     * 生成Token
     * @Author Sans
     * @CreateTime 2019/10/2 12:16
     * @Param  selfUserEntity 用户安全实体
     * @Return Token
     */
    public String createAccessToken(SecurityUser securityUser){
        // 登陆成功生成JWT
        String token = Jwts.builder()
                // 放入用户名和用户ID
                .setId(securityUser.getUsername()+"")
                // 主题
                .setSubject(securityUser.getUsername())
                // 签发时间
                .setIssuedAt(new Date())
                // 签发者
                .setIssuer("sans")
                // 自定义属性 放入用户拥有权限
                .claim("authorities", JSON.toJSONString(securityUser.getAuthorities()))
                // 失效时间
                .setExpiration(new Date(System.currentTimeMillis() + tokenProperties.getTokenExpireSecond()))
                // 签名算法和密钥
                .signWith(SignatureAlgorithm.HS512, tokenProperties.getSecretKey())
                .compact();
        return token;
    }

}
