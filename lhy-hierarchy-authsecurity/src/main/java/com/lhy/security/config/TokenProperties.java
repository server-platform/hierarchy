package com.lhy.security.config;

import lombok.Data;

/**
 * @author libo
 */
@Data
public class TokenProperties {
    private String secretKey;
    private Long tokenExpireSecond;
    private String tokenHeaderPrefix;
    private String authorizationHeaderName;
    private Long refreshTokenExpiredSecond;
    private String refreshHeaderName;
    private String userId;
}
