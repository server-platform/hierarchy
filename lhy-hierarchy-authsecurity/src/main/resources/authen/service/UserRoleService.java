package com.hyts.delayer.authen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hyts.delayer.authen.entity.UserRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author testjava
 * @since 2020-01-12
 */
public interface UserRoleService extends IService<UserRole> {

}
