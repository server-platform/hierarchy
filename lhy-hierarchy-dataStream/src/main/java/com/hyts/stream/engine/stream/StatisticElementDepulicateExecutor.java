/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.stream.engine.stream;

import com.hyts.stream.engine.model.WordEvent;
import com.hyts.stream.engine.source.CustomSource;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.state.ValueState;
import org.apache.flink.api.common.state.ValueStateDescriptor;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessAllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

/**
 * @project-name:lhy-stream
 * @package-name:com.hyts.stream.engine.stream
 * @author:LiBo/Alex
 * @create-date:2022-05-15 18:46
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class StatisticElementDepulicateExecutor {




    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setStreamTimeCharacteristic(TimeCharacteristic.ProcessingTime);
        env.addSource(new CustomSource())
                .keyBy(e -> e.getWord())
                .timeWindow(Time.seconds(3))
//                .assignTimestampsAndWatermarks(WatermarkStrategy.forMonotonousTimestamps())
                .process(new ProcessWindowFunction<WordEvent, Object, String, TimeWindow>() {
                    ValueState<Boolean> keyHasBeenSeen;
                    @Override
                    public void open(Configuration parameters) throws Exception {
                        ValueStateDescriptor<Boolean> desc = new ValueStateDescriptor<>("keyHasBeenSeen", Types.BOOLEAN);
                        keyHasBeenSeen = getRuntimeContext().getState(desc);
                    }

                    @Override
                    public void process(String s, ProcessWindowFunction<WordEvent, Object, String, TimeWindow>.Context context, Iterable<WordEvent> elements, Collector<Object> out) throws Exception {
                        WordEvent event = elements.iterator().next();
                        System.out.println("orgin the event model:"+event+"----"+keyHasBeenSeen.value());
                        if (keyHasBeenSeen.value() == null) {
                            keyHasBeenSeen.update(true);
                            out.collect(event);
                            System.out.println("output the model:"+event);
                        }else{
                            System.out.println("already exist the event model:"+event.getWord()+"----");
                        }
                    }

                }).addSink(new RichSinkFunction<Object>() {
                    @Override
                    public void invoke(Object value, Context context) throws Exception {
                        System.out.println("---sink:"+value);
                    }
                });
        env.execute();

    }

    public static class Deduplicator extends RichFlatMapFunction<WordEvent, WordEvent> {

        ValueState<Boolean> keyHasBeenSeen;

        @Override
        public void open(Configuration conf) {
            ValueStateDescriptor<Boolean> desc = new ValueStateDescriptor<>("keyHasBeenSeen", Types.BOOLEAN);
            keyHasBeenSeen = getRuntimeContext().getState(desc);
        }

        @Override
        public void flatMap(WordEvent event, Collector<WordEvent> out) throws Exception {
            System.out.println("orgin the event model:"+event+"----"+keyHasBeenSeen.value());
            if (keyHasBeenSeen.value() == null) {
                keyHasBeenSeen.update(true);
                out.collect(event);
                System.out.println("output the model:"+event);
            }else{
                System.out.println("already exist the event model:"+event.getWord()+"----");
            }
        }
    }


}
