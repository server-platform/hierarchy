/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hyts.stream.engine.stream;

import cn.hutool.core.date.DateUtil;
import com.hyts.stream.engine.model.WordEvent;
import com.hyts.stream.engine.source.CustomSource;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.Date;
import java.util.Iterator;

/**
 * @project-name:lhy-stream
 * @package-name:com.hyts.stream.engine.sample
 * @author:LiBo/Alex
 * @create-date:2022-05-13 00:07
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class ScrollStreamExecutor {




    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.addSource(new CustomSource())
                .keyBy(WordEvent::getWord)
                .window(TumblingProcessingTimeWindows.of(Time.seconds(3))).
                 process(new ProcessWindowFunction<WordEvent, WordEvent, String, TimeWindow>(){
                    @Override
                    public void process(String s, ProcessWindowFunction<WordEvent, WordEvent, String, TimeWindow>.Context context,
                                        Iterable<WordEvent> elements, Collector<WordEvent> out) throws Exception {
                        Iterator iterator = elements.iterator();
                        while(iterator.hasNext()){
                            WordEvent event = (WordEvent) iterator.next();
                            System.out.println("---process----"+event);
                            out.collect(event);
                        }
                    }
                })
                .flatMap(new FlatMapFunction<WordEvent, Object>() {
                    @Override
                    public void flatMap(WordEvent value, Collector<Object> out) throws Exception {
                        out.collect(value);
                        value.setCount(value.getCount()+1);
                        out.collect(value);
                    }
                }).setParallelism(2).
                addSink(new RichSinkFunction<Object>() {
                        @Override
                        public void invoke(Object value, Context context) throws Exception {
                            System.out.println(((WordEvent)value).getWord()+"-----rest:"+value);
                        }
                });
//                .apply(new WindowFunction<WordEvent, WordEvent, String, TimeWindow>() {
//                    @Override
//                    public void apply(String s, TimeWindow window, Iterable<WordEvent> input, Collector<WordEvent> out) throws Exception {
//                        for (WordEvent word : input) {
//                            System.out.println(word);
//                            out.collect(word);
//                        }
//                    }
//                })

        env.execute();
    }
}
