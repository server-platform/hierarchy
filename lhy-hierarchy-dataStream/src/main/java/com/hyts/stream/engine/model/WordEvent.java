package com.hyts.stream.engine.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTimeUtils;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import java.text.DateFormat;
import java.util.Date;

/**
 * Desc:
 * Created by zhisheng on 2019-08-07
 * blog：http://www.54tianzhisheng.cn/
 * 微信公众号：zhisheng
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WordEvent {
    private String word;

    private int count;

    private int count2;

    private long timestamp;

    private MetricEvent event;

    private String format;

    public WordEvent(String word, int count, long timestamp) {
        this.word = word;
        this.count = count;
        this.timestamp = timestamp;
    }

    public WordEvent(String word, int count, int count2, long timestamp) {
        this.word = word;
        this.count = count;
        this.count2 = count2;
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "WordEvent{" + "word='" + word + '\'' + ", count=" + count + ", count2=" + count2 + ", timestamp=" + DateFormat.getDateTimeInstance().
                format(new Date(timestamp))  + ", event=" + event + ", format='" + format + '\'' + '}';
    }
}
