
create table execute_task_info(
                                  execute_task_id       bigint auto_increment
                                      primary key,
                                  execute_task_code     varchar(64) default ''                 not null
                                      comment '执行任务编码',
                                  execute_biz_code      varchar(64) default ''                 not null
                                      comment '业务编码，用于与业务数据进行关联使用！',
                                  execute_task_type     tinyint default '-1'                   not null
                                      comment '可以作为分区键
-1:未划分类：属于默认出口类型！',
                                  execute_task_status   tinyint default '0'                    not null
                                      comment '0:未完成（待执行）
1:已完成
2:执行失败
3:暂停状态',
                                  execute_task_name     varchar(128) default ''                not null
                                      comment '任务名称机制',
                                  execute_task_duration bigint default '0'                     not null
                                      comment '导出时间消耗时常',
                                  execute_task_result   varchar(255) default ''                not null
                                      comment '执行任务的内容：例如：导出操作为：下载文件地址路径
',
                                  execute_start_time    datetime                               null
                                      comment '任务实际执行时间！',
                                  execute_stop_time     datetime                               null
                                      comment '任务实际执行时间！',
                                  execute_input_param   json                                   null,
                                  is_delete             tinyint default '0'                    not null,
                                  department_id         bigint default '0'                     not null,
                                  enterprise_id         bigint default '0'                     not null,
                                  user_id               bigint default '0'                     not null,
                                  update_time           datetime default CURRENT_TIMESTAMP     not null,
                                  create_time           datetime default CURRENT_TIMESTAMP     not null,
                                  result_data_size      bigint default '0'                     not null,
                                  result_expire_time    datetime default '2099-12-31 00:00:00' null
                                      comment '数据保留的有效时间！',
                                  failure_retry_count   int default '0'                        null
                                      comment '失败重试次数！',
                                  failure_reason        varchar(255) default ''                null,
                                  constraint execute_task_info_execute_task_code_uindex
                                      unique (execute_task_code)
)
    comment '任务执行信息（抽象复用-分区分类处理）';

create index execute_task_info_execute_task_status_index
    on execute_task_info (execute_task_status);

create index execute_task_info_execute_task_type_index
    on execute_task_info (execute_task_type);

create index execute_task_info_user_id_index
    on execute_task_info (user_id);
