/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.web;

import com.lhy.report.bean.MigrationDataParam;
import com.lhy.report.schedule.DynamicTaskScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.web
 * @author:LiBo/Alex
 * @create-date:2021-10-02 7:28 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@RestController
@RequestMapping("/")
public class ReportTaskController {


    @Autowired
    DynamicTaskScheduler scheduler;


    @Autowired
    RedisTemplate redisTemplate;


    @RequestMapping(value = "/add",method = RequestMethod.GET)
    public void addReportTask(){
        System.out.println("12313113");
        scheduler.addReportTask(()->{
            System.out.println("12313113");
        },"*/2 * * * * ?");
    }


    @RequestMapping(value = "/cache",method = RequestMethod.GET)
    public void addCache(){
        MigrationDataParam migrationDataParam = new MigrationDataParam();
        migrationDataParam.setAllCount(1L);
        migrationDataParam.setProcessRateValue(22);
        migrationDataParam.setDataStatus((byte) 2);
        migrationDataParam.setDataRangeType(MigrationDataParam.MigrationDataRangeType.MIGRATION_TARGET_IDS);
        redisTemplate.opsForList().leftPush("test",migrationDataParam);
    }


    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String get(){
        Object result =  redisTemplate.opsForList().rightPop("EXECUTE_PROCESS:0", 5, TimeUnit.SECONDS);
        return String.valueOf(result);
    }




}
