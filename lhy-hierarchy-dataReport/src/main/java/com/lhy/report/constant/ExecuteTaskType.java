/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.constant;

/**
 * @project-name:wiz-sound-ai2
 * @package-name:com.wiz.soundai.task.constant
 * @author:LiBo/Alex
 * @create-date:2021-09-29 11:54
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public interface ExecuteTaskType {

    /**
     * 获取对应的ExecuteTaskType
     * @param code
     * @return
     */
    ExecuteTaskType getMeByTypeCode(byte code);

    /**
     * 类型编码
     * @return
     */
    byte getType();

    /**
     * 名称
     * @return
     */
    String getMessage();

    /**
     * 执行成功后的信息
     * @return
     */
    String getSuccessMessage();


}