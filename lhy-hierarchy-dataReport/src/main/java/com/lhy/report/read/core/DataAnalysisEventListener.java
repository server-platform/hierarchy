/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.read.core;

import cn.hutool.core.date.StopWatch;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.concurrent.ThreadSafe;
import java.util.List;
import java.util.Objects;
import java.util.function.BiFunction;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.read
 * @author:LiBo/Alex
 * @create-date:2021-09-10 18:51
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@ThreadSafe
@Slf4j
public class DataAnalysisEventListener<T> extends AnalysisEventListener<T> {

    /**
     * 操作总体耗时
     */
    private long processCostTime;

    /**
     * 数据总量
     */
    private int dataTotalNum;

    /**
     * 当前处理的数据总量
     */
    private int currentDataNum;

    /**
     * 批次数据大小
     */
    private int batchSize = 1<<5;

    /**
     * 内部数据集合信息
     * 可以通过实例获取该值
     */
    @Getter
    private List<T> dataList;


    /**
     * 是否支持异步操作控制
     */
    private boolean async;

    /**
     * 当前技术器控制信息
     */
    private int currentIndex = 0 ;


    private StopWatch stopWatch = new StopWatch();


    /**
     * 操作执行文件处理机制
     */
    private BiFunction<Object,AnalysisContext,Object> processHandler;

    /**
     * 构造器
     */
    public DataAnalysisEventListener() {
        this(1<<5);
    }

    /**
     * 构造器
     * @param batchSize
     */
    public DataAnalysisEventListener(int batchSize) {
        this(0,batchSize);
    }

    /**
     * 构造器(可以用于统计当前计算得出的百分比值)
     * @param dataTotalNum
     * @param batchSize
     */
    public DataAnalysisEventListener(int dataTotalNum, int batchSize) {
        this(0,batchSize,Boolean.FALSE);
    }

    /**
     * 执行先关处理控制器
     * @param dataTotalNum
     * @param batchSize
     * @param async
     */
    public DataAnalysisEventListener(int dataTotalNum, int batchSize,boolean async) {
        this(dataTotalNum,batchSize,async,null);
    }


    /**
     *
     * @param batchSize
     * @param async
     * @param processHandler
     */
    public DataAnalysisEventListener(int batchSize,boolean async,BiFunction<Object,AnalysisContext,Object>
            processHandler) {
        this(0,batchSize,async,processHandler);
    }


    /**
     *
     * @param batchSize
     * @param processHandler
     */
    public DataAnalysisEventListener(int batchSize,BiFunction<Object,AnalysisContext,Object>
            processHandler) {
        this(0,batchSize,Boolean.FALSE,processHandler);
    }

    /**
     * 构造器
     * @param dataTotalNum
     * @param batchSize
     * @param async
     * @param processHandler
     */
    public DataAnalysisEventListener(int dataTotalNum, int batchSize,boolean async,BiFunction<Object,AnalysisContext,Object>
            processHandler) {
        this.dataTotalNum = dataTotalNum;
        this.batchSize = batchSize;
        //此部分的意思是，如果batchSize小于0，则说明不进行分割计算
        this.dataList = Lists.newArrayListWithExpectedSize(batchSize<0?dataTotalNum:batchSize);
        //是否支持异步操作
        this.async = async;
        this.processHandler = processHandler;
    }


    /**
     * 执行调用机制
     * @param o
     * @param analysisContext
     */
    @Override
    public void invoke(T o, AnalysisContext analysisContext) {
        //操作控制
        if(Objects.isNull(processHandler)){
            dataTotalNum++;
            log.info("数据处理器为空，无法执行，直接丢弃相关数据信息模型");
            return ;
        }
        if(!stopWatch.isRunning()) {
            stopWatch.start();
        }
        //如果批次数目<=0，则直接进行相关的赋值，不进行创建对象
        if(batchSize == 0){
            //如果是异步操作，则支持先关的disruptor的控制机制实现异步数据处理操作
            if(async){

            }
            processHandler.apply(o,analysisContext);
        }else{
            fullCurrentIndexAndData(o);
            if(batchSize >= currentIndex){
                processHandler.apply(o,analysisContext);
                resetCurrentIndexAndData();
            }
        }
    }

    /**
     *
     * @param analysisContext
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        dataList.clear();
        stopWatch.stop();
        processCostTime = stopWatch.getTotalTimeMillis();
    }


    /**
     * 填充数据信息控制
     * @param element
     */
    private void fullCurrentIndexAndData(T element){
        dataList.add(element);
        currentIndex++;
    }

    /**
     * 重置数据
     */
    private void resetCurrentIndexAndData(){
        currentIndex = 0;
        dataList.clear();
    }




}
