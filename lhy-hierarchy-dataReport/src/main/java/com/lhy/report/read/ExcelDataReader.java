/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.read;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.read.builder.ExcelReaderBuilder;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.lhy.report.read.core.DataAnalysisEventListener;

import java.io.InputStream;
import java.util.function.BiFunction;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.read
 * @author:LiBo/Alex
 * @create-date:2021-09-10 21:31
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class ExcelDataReader {


    public static void read(String excelFilePath,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(excelFilePath,Boolean.TRUE,new DataAnalysisEventListener(1<<8,Boolean.FALSE,processHandler),sheetName);
    }

    public static void read(InputStream inputStream,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(inputStream,Boolean.TRUE,new DataAnalysisEventListener(1<<8,Boolean.FALSE,processHandler),sheetName);
    }

    public static void read(String excelFilePath,int batchSize,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(excelFilePath,Boolean.TRUE,new DataAnalysisEventListener(batchSize,Boolean.FALSE,processHandler),sheetName);
    }

    public static void read(InputStream inputStream,int batchSize,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(inputStream,Boolean.TRUE,new DataAnalysisEventListener(batchSize,Boolean.FALSE,processHandler),sheetName);
    }

    public static void read(String excelFilePath,int batchSize,boolean async,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(excelFilePath,Boolean.TRUE,new DataAnalysisEventListener(batchSize,async,processHandler),sheetName);
    }


    public static void read(InputStream inputStream,int batchSize,boolean async,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(inputStream,Boolean.TRUE,new DataAnalysisEventListener(batchSize,async,processHandler),sheetName);
    }


    public static void read(String excelFilePath,int dataTotalNum, int batchSize,boolean async,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(excelFilePath,Boolean.TRUE,new DataAnalysisEventListener(dataTotalNum,batchSize,async,processHandler),sheetName);
    }


    public static void read(InputStream inputStream, int dataTotalNum, int batchSize,boolean async,
                            BiFunction<Object, AnalysisContext,Object> processHandler,String sheetName){
        read(inputStream,Boolean.TRUE,new DataAnalysisEventListener(dataTotalNum,batchSize,async,processHandler),sheetName);
    }


    public static void read(String excelFilePath,
                            AnalysisEventListener analysisEventListener,String sheetName){
        read(excelFilePath,Boolean.TRUE,analysisEventListener,sheetName);
    }


    public static void read(InputStream inputStream,
                            AnalysisEventListener analysisEventListener,String sheetName){
        read(inputStream,Boolean.TRUE,analysisEventListener,sheetName);
    }



    public static void read(String excelFilePath, boolean ignoreEmptyRow,
                            AnalysisEventListener analysisEventListener,String sheetName){
        new ExcelReaderBuilder().file(excelFilePath).
                ignoreEmptyRow(ignoreEmptyRow).
                excelType(ExcelTypeEnum.XLSX).
                registerReadListener(analysisEventListener).
                sheet(sheetName).doRead();

    }


    public static void read(InputStream inputStream, boolean ignoreEmptyRow, AnalysisEventListener analysisEventListener,
                            String sheetName){
        new ExcelReaderBuilder().file(inputStream).
                ignoreEmptyRow(ignoreEmptyRow).
                excelType(ExcelTypeEnum.XLSX).
                registerReadListener(analysisEventListener).
                sheet(sheetName)
                .doRead();
    }


    public static void read(String excelFilePath, boolean ignoreEmptyRow,
                            AnalysisEventListener analysisEventListener){
        new ExcelReaderBuilder().file(excelFilePath).
                ignoreEmptyRow(ignoreEmptyRow).
                excelType(ExcelTypeEnum.XLSX).
                registerReadListener(analysisEventListener).
                sheet(0).
                doRead();
    }



    public static void read(InputStream inputStream, boolean ignoreEmptyRow, AnalysisEventListener analysisEventListener){
        new ExcelReaderBuilder().file(inputStream).
                ignoreEmptyRow(ignoreEmptyRow).
                excelType(ExcelTypeEnum.XLSX).
                registerReadListener(analysisEventListener).
                sheet(0).
                doRead();
    }



}
