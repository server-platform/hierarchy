package report.shiro;

import org.springframework.cache.annotation.CachingConfigurerSupport;


//@Configuration
//@EnableCaching
public class RedisConfig extends CachingConfigurerSupport {

//
//
//    @Bean
//    public KeyGenerator wiselyKeyGenerator(){
//        return (target, method, params) -> {
//            StringBuilder sb = new StringBuilder();
//            sb.append(target.getClass().getName());
//            sb.append(method.getName());
//            for (Object obj : params) {
//                sb.append(obj.toString());
//            }
//            return sb.toString();
//        };
//    }
//
////    @Bean
////    public CacheManager cacheManager(RedisTemplate redisTemplate) {
////
////        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(lettuceConnectionFactory);
////
////        FastJson2JsonRedisSerializer serializer = new FastJson2JsonRedisSerializer();
////        RedisSerializationContext.SerializationPair<Object> pair =
////                RedisSerializationContext.SerializationPair.fromSerializer(serializer);
////        RedisCacheConfiguration defaultCacheConfig = RedisCacheConfiguration.defaultCacheConfig().serializeValuesWith(pair);
////        //设置过期时间 30天
////        defaultCacheConfig = defaultCacheConfig.entryTtl(Duration.ofDays(30));
////        //初始化RedisCacheManager
////        RedisCacheManager cacheManager = new RedisCacheManager(redisCacheWriter, defaultCacheConfig);
////        //反序列化白名单
////        ParserConfig.getGlobalInstance().addAccept("cn.seaboot.admin.bean.");
////        return cacheManager;
////        return new RedisCacheManager(redisTemplate);
////    }
//
//
//	@Bean
//    public RedisTemplate<String, String> redisTemplate(
//            RedisConnectionFactory factory) {
//        StringRedisTemplate template = new StringRedisTemplate(factory);
//        Jackson2JsonRedisSerializer jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer(Object.class);
//        ObjectMapper om = new ObjectMapper();
//        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
//        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
//        jackson2JsonRedisSerializer.setObjectMapper(om);
//        template.setValueSerializer(jackson2JsonRedisSerializer);
//        template.afterPropertiesSet();
//        return template;
//    }

}  