package report.shiro;

import org.apache.shiro.spring.LifecycleBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShiroConfig {
	
//	/**
//	 * FilterRegistrationBean
//	 * @return
//	 */
//	@Bean
//	public FilterRegistrationBean filterRegistrationBean() {
//		FilterRegistrationBean filterRegistration = new FilterRegistrationBean();
//        filterRegistration.setFilter(new DelegatingFilterProxy("shiroFilter"));
//        filterRegistration.setEnabled(true);
//        filterRegistration.addUrlPatterns("/*");
//        filterRegistration.setDispatcherTypes(DispatcherType.REQUEST);
//        return filterRegistration;
//	}
//
//	/**
//	 * @see org.apache.shiro.spring.web.ShiroFilterFactoryBean
//	 * @return
//	 */
//	@Bean(name = "shiroFilter")
//	public ShiroFilterFactoryBean shiroFilter(){
//		ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
//		bean.setSecurityManager(securityManager());
//		bean.setLoginUrl("/login");
//		bean.setUnauthorizedUrl("/unauthor");
//		Map<String, Filter>filters = Maps.newHashMap();
//		filters.put("anon", new AnonymousFilter());
//		bean.setFilters(filters);
//		Map<String, String> chains = Maps.newHashMap();
//		chains.put("/login", "anon");
//		chains.put("/unauthor", "anon");
//		chains.put("/logout", "logout");
//		chains.put("/base/**", "anon");
//		chains.put("/css/**", "anon");
//		chains.put("/layer/**", "anon");
//		chains.put("/**", "authc,perms");
//		bean.setFilterChainDefinitionMap(chains);
//		return bean;
//	}
//
//	/**
//	 * @see org.apache.shiro.mgt.SecurityManager
//	 * @return
//	 */
//	@Bean(name="securityManager")
//	public DefaultWebSecurityManager securityManager() {
//		DefaultWebSecurityManager manager = new DefaultWebSecurityManager();
//		manager.setRealm(userRealm());
//		manager.setCacheManager(redisCacheManager());
//		manager.setSessionManager(defaultWebSessionManager());
//		return manager;
//	}
//
//	/**
//	 * @see DefaultWebSessionManager
//	 * @return
//	 */
//	@Bean(name="sessionManager")
//	public DefaultWebSessionManager defaultWebSessionManager() {
//		DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
//		sessionManager.setCacheManager(redisCacheManager());
//		sessionManager.setGlobalSessionTimeout(1800000);
//		sessionManager.setDeleteInvalidSessions(true);
//		sessionManager.setSessionValidationSchedulerEnabled(true);
//		sessionManager.setDeleteInvalidSessions(true);
//		return sessionManager;
//	}
//
	/**
	 * @see UserRealm--->AuthorizingRealm
	 * @return
	 */
//	@Bean
//	@DependsOn(value={"lifecycleBeanPostProcessor", "shrioRedisCacheManager"})
//	public UserRealm userRealm() {
//		UserRealm userRealm = new UserRealm();
//		userRealm.setCacheManager(redisCacheManager());
//		userRealm.setCachingEnabled(true);
//		userRealm.setAuthenticationCachingEnabled(true);
//		userRealm.setAuthorizationCachingEnabled(true);
//		return userRealm;
//	}
	

//	@Bean(name="shrioRedisCacheManager")
//	@DependsOn(value="redisTemplate")
//	public ShrioRedisCacheManager redisCacheManager() {
//		ShrioRedisCacheManager cacheManager = new ShrioRedisCacheManager(redisTemplate());
//		return cacheManager;
//	}


//	@Bean(name="redisTemplate")
//	public RedisTemplate<byte[], Object> redisTemplate() {
//	    RedisTemplate<byte[], Object> template = new RedisTemplate<>();
//	    template.setConnectionFactory(connectionFactory());
//	    return template;
//	}


//	@Bean
//	public JedisConnectionFactory connectionFactory(){
//		JedisConnectionFactory conn = new JedisConnectionFactory();
//		RedisCfg cfg = redisCfg();
//		conn.setDatabase(cfg.getDatabase());
//		conn.setHostName(cfg.getHost());
//		conn.setPassword(cfg.getPassword());
//		conn.setPort(cfg.getPort());
//		conn.setTimeout(cfg.getTimeout());
//		return conn;
//	}
//

	@Bean
	public LifecycleBeanPostProcessor lifecycleBeanPostProcessor() {
		return new LifecycleBeanPostProcessor();
	}
}