/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.tools;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.tools
 * @author:LiBo/Alex
 * @create-date:2021-09-16 22:59
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public interface DateRangeSplitter {


    DateFormat DATE_TIME_FORMATTER = DateFormat.getDateTimeInstance();


    /**
     * 时间范围分片对象
     */
    @AllArgsConstructor
    @RequiredArgsConstructor
    @Data
    class DateRangeSplit{

        /**
         * 序号
         */
        private int seqNumber;

        /**
         * 分片开始时间
         */
        private  Date splitStartDate;

        /**
         * 分片结束事件
         */
        private  Date splitEndDate;



        @Override
        public String toString() {
            return "DateRangeSplit{" + "seqNumber=" + seqNumber + ", splitStartDate=" + DATE_TIME_FORMATTER.
                    format(splitStartDate) + ", splitEndDate=" + DATE_TIME_FORMATTER.format(splitEndDate) + '}';
        }
    }


    /**
     * 时间范围分片机制（根据单元分片）
     * @param startDate
     * @param endDate
     * @return
     */

    List<DateRangeSplit> splitDateByUnit(LocalDateTime startDate, LocalDateTime endDate);


    /**
     * 时间范围分片机制（根据数量分片）
     * @param startDate
     * @param endDate
     * @return
     */
    List<DateRangeSplit> splitDateByNum(LocalDateTime startDate, LocalDateTime endDate);

}
