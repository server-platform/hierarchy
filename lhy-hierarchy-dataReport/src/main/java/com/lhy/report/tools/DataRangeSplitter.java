/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.tools;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @project-name:wiz-sound-ai2
 * @package-name:com.wiz.soundai.common.service
 * @author:LiBo/Alex
 * @create-date:2021-10-08 15:53
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 *
 */
public interface DataRangeSplitter {


    /**
     * 数据分片区域模型类
     */
    @ToString
    @AllArgsConstructor
    @Data
    class DataNumberRange{

        /**
         * 顺序执行编号
         */
        private int seqNumber;

        /**
         * 开始坐标
         */
        private long startIndex;

        /**
         * 结束坐标
         */
        private long endIndex;
    }

    /**
     * 时间范围分片机制（根据单元分片）
     * @param number 数据总数
     * @param numberUnit 数据单位！
     * @return
     */
    List<DataNumberRange> splitDataByUnit(long number, int numberUnit);

    /**
     * 直接推送数据信息数量
     * @return
     */
    List<Long> splitDataNumberByUnit(long number, int numberUnit);


    /**
     * 操作处理机制
     * @param number
     * @param numberUnit
     */
    default void valiateNumberRelation(long number, int numberUnit){
        if(number<=0 || numberUnit<=0  ){
            throw new IllegalArgumentException("the split data is not be negtative number ~");
        }
    }

}
