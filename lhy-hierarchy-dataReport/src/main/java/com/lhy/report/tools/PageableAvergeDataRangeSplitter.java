/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.tools;


import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @project-name:wiz-sound-ai2
 * @package-name:com.wiz.soundai.common.service
 * @author:LiBo/Alex
 * @create-date:2021-10-08 16:02
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 分页模式下的数据模型接口机制控制！
 */
public class PageableAvergeDataRangeSplitter implements DataRangeSplitter {


    /**
     * 查询进行数据分割操作
     * @param number 数据总数
     * @param numberUnit 数据单位！
     * @return
     */
    @Override
    public List<DataNumberRange> splitDataByUnit(long number, int numberUnit) {
        valiateNumberRelation(number,numberUnit);
        // 如果总数小于等于分片单元则直接进行返回相关数据信息控制
        List<DataNumberRange> dataNumberRanges = null;
        if(number <= numberUnit){
            dataNumberRanges = new ArrayList<>(1);
            dataNumberRanges.add(new DataNumberRange(0,0L,numberUnit-1L));
        }else{
            dataNumberRanges = new ArrayList<>();
            // 自增计算进行分割数据数据信息
            for(int currentStartIndex = 0,seqNumber = 0; currentStartIndex < number; seqNumber++){
                dataNumberRanges.add(new DataNumberRange(seqNumber,currentStartIndex,
                        Math.min((currentStartIndex+=numberUnit)-1L,number-1L)));
            }
        }
        return dataNumberRanges;
    }


    /**
     * 查询进行数据分割操作
     * @param number
     * @param numberUnit
     * @return
     */
    @Override
    public List<Long> splitDataNumberByUnit(long number, int numberUnit) {
        List<DataNumberRange> dataNumberRanges =  splitDataByUnit(number,numberUnit);
        if(CollectionUtils.isNotEmpty(dataNumberRanges)){
            return dataNumberRanges.stream().map(param->(param.getEndIndex() - param.getStartIndex())+1).collect(Collectors.toList());
        }
        return new ArrayList<Long>(){{add(0L);}};
    }

}

