/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.tools;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.tools
 * @author:LiBo/Alex
 * @create-date:2021-09-17 01:10
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class SimpleRateLimiter {




     public static void main(String[] args){

             String fromDays = "2021-12-12",toDays = "2021-12-22";
             DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
             LocalDateTime startDate = LocalDateTime.from(dateTimeFormatter.parse(fromDays+" 00:00:00"));
             LocalDateTime endDate = LocalDateTime.from(dateTimeFormatter.parse(toDays+" 23:59:59"));
             System.out.println(Duration.between(startDate,endDate).toDays());
             System.out.println(Duration.between(startDate,endDate).toHours()/24.0);




//         ExecutorService service = ThreadUtil.newExecutor(10,20);
//
//         CompletionService<Integer> completionService = ThreadUtil.newCompletionService(service);
//
//         //创建限流工具，每秒发出15个令牌指令
////         RateLimiter rateLimiter = RateLimiter.create(100,500, TimeUnit.MILLISECONDS);
//
//
//         Future<Integer> result = completionService.submit(() -> {
//             int localVaribale = 0;
//             RateLimiter rateLimiter = RateLimiter.create(100,500, TimeUnit.MILLISECONDS);
//             for(int i = 0 ; i< 10;i++){
//                 rateLimiter.acquire(1);
//                 System.out.println("1");
//                 localVaribale+=1;
//             }
//             return localVaribale;
//         });
//
//         Future<Integer> result2 = completionService.submit(() -> {
//             int localVaribale = 0;
//             RateLimiter rateLimiter = RateLimiter.create(1,500, TimeUnit.MILLISECONDS);
//             for(int i = 0 ; i< 10;i++){
//                 rateLimiter.acquire(1);
//                 System.out.println("2");
//                 localVaribale+=1;
//             }
//             return localVaribale;
//         });
//
//         Future<Integer> result3 = completionService.submit(() -> {
//             int localVaribale = 0;
//             RateLimiter rateLimiter = RateLimiter.create(1,500, TimeUnit.MILLISECONDS);
//             for(int i = 0 ; i< 10;i++){
//                 rateLimiter.acquire(1);
//                 System.out.println("3");
//                 localVaribale+=1;
//             }
//             return localVaribale;
//         });
//
//         List<Future<Integer>> resultList = Lists.newArrayList(result,result2,result3);
////
////         int sum = resultList.stream().mapToInt(param-> {
////             try {
////                 return param.get();
////             } catch (InterruptedException e) {
////                 e.printStackTrace();
////             } catch (ExecutionException e) {
////                 e.printStackTrace();
////             }
////             return 0;
////         }).sum();
////         System.out.println(sum);
////
////
////         for(int i = 0 ; i<2 ; i++){
////             try {
////                 int result23 = completionService.take().get();
////                 System.out.println(result23);
////             } catch (InterruptedException e) {
////                 e.printStackTrace();
////             } catch (ExecutionException e) {
////                 e.printStackTrace();
////             }
////         }
//
//         int sum2 = resultList.stream().mapToInt(param-> {
//             try {
//                 return completionService.take().get();
//             } catch (InterruptedException e) {
//                 e.printStackTrace();
//             } catch (ExecutionException e) {
//                 e.printStackTrace();
//             }
//             return 0;
//         }).sum();
//
//         System.out.println(sum2);
     }


}
