/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.fallback;

/**
 * @project-name:wiz-sound-ai6
 * @package-name:com.wiz.soundai.callservice.gateway.manager.async
 * @author:LiBo/Alex
 * @create-date:2021-11-16 10:39
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 异步数据处理控制服务
 */
public interface FallbackDataProcessStrategy<P,R>{

    /**
     * 异步数据处理服务
     * @param param
     * @return
     */
    R process(P param);

    /**
     * 操作处理控制机制
     * @return
     */
    R fallback(Exception e,P param);



}
