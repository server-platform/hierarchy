/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.fallback;

/**
 * @project-name:wiz-sound-ai6
 * @package-name:com.wiz.soundai.callservice.gateway.manager.fallback
 * @author:LiBo/Alex
 * @create-date:2021-11-16 12:46
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 重试Process处理策略机制控制
 */
public interface RetryFallbackProcessStrategy<T,PROCESS>{


    /**
     * 包含执行重试操作处理
     * @param param
     * @return
     */
    boolean process(T param,PROCESS process);

    /**
     * 是否应该执行重试
     * @param param
     * @return
     */
    boolean shouldRetry(T param,Boolean lastResult);


    /**
     * 是否需要进行告警操作
     * @param param
     * @return
     */
    boolean warning(T param);

}
