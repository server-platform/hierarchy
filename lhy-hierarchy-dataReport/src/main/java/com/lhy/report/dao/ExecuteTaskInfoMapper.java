package report.dao;

import com.lhy.report.bean.execute.ExecuteTaskInfo;
import com.lhy.report.bean.execute.ExecuteTaskInfoExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExecuteTaskInfoMapper {
    long countByExample(ExecuteTaskInfoExample example);

    int deleteByExample(ExecuteTaskInfoExample example);

    int deleteByPrimaryKey(Long executeTaskId);

    int insert(ExecuteTaskInfo record);

    int insertSelective(ExecuteTaskInfo record);

    List<ExecuteTaskInfo> selectByExample(ExecuteTaskInfoExample example);

    ExecuteTaskInfo selectByPrimaryKey(Long executeTaskId);

    int updateByExampleSelective(@Param("record") ExecuteTaskInfo record, @Param("example") ExecuteTaskInfoExample example);

    int updateByExample(@Param("record") ExecuteTaskInfo record, @Param("example") ExecuteTaskInfoExample example);

    int updateByPrimaryKeySelective(ExecuteTaskInfo record);

    int updateByPrimaryKey(ExecuteTaskInfo record);
}