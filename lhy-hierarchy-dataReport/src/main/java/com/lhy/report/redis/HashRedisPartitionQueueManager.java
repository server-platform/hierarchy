/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.redis;

import cn.hutool.core.thread.ThreadUtil;
import com.google.common.base.Preconditions;
import com.lhy.report.bean.HashQueueElement;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.IntStream;

/**
 * @project-name:wiz-sound-ai2
 * @package-name:com.wiz.soundai.task.config.redis
 * @author:LiBo/Alex
 * @create-date:2021-09-24 16:38
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 简单的分区队列控制执行操作服务管理器
 */

@Slf4j
@Component
public class HashRedisPartitionQueueManager {

    /**
     * 数据信息
     */
    public static final int CURRENT_EXECUTE_DO_SIZE = Runtime.getRuntime().availableProcessors();
//Math.ceil(
////        // Num IO threads (top out at 4, since most I/O devices won't scale better than this)
////        Math.min(4.0, Runtime.getRuntime().availableProcessors() * 0.75) +
////            // Num scanning threads (higher than available processors, because some threads can be blocked)
////            Runtime.getRuntime().availableProcessors() * 1.25)
    /**
     * 缩编化的数据信息
     */
    public static final int CURRENT_EXECUTE_DO_SIZE_SW = Runtime.getRuntime().availableProcessors()>>1;



    public static final String HASH_SEQ_NUMBER_1 = "CONTACT_INFO_PUSH_COUNTER";


    public static final String HASH_SEQ_NUMBER_2 = "CONTACT_INFO_POLL_COUNTER";


    @Autowired
    ApplicationContext applicationContext;


    @Getter
    private RedisTemplate<String, Object> redisTemplate;



    @PostConstruct
    public void initRedisTemplate(){
        try {
            redisTemplate = applicationContext.getBean("redisTemplate",RedisTemplate.class);
        } catch (BeansException e) {
            log.error("init the redistemplate is faliure!");
        }
    }

    /**
     * 工厂方法
     * @param BizNo
     * @param element
     * @param <T>
     * @return
     */
    public static <T> HashQueueElement createHashQueueElement(Long BizNo, T element){
        // TODO 未来可以实现拷贝，提高创建速度     @Contended 防止伪共享
        return new HashQueueElement(BizNo,element);
    }


    /**
     * add阻塞队列元素控制机制
     * @param key
     * @param entity
     * @param <T>
     */
    public <T> Long addBlockingElement(String key,  T entity) {
        Long length = redisTemplate.opsForList().leftPush(key, entity);
        Long expireTime = redisTemplate.getExpire(key, TimeUnit.SECONDS);
        if(Objects.nonNull(expireTime) && expireTime < 60){
            log.debug("key:{}  ttl  {} is less than 60 seconds will be update ttl to long",key,expireTime);
            redisTemplate.expire(key, 3, TimeUnit.HOURS);
        }
        // 添加计数器操作控制
        try {
            redisTemplate.opsForHash().increment(HASH_SEQ_NUMBER_1,key,1L);
        } catch (Exception e) {
            log.error("add hash counter is failure!",e);
        }
        return length;
    }

    /**
     * get阻塞队列元素控制机制
     * @param key
     */
    public <T> T popBlockingElement(String key) {
        // 添加计数器操作控制
        return (T) redisTemplate.opsForList().rightPop(key, 3, TimeUnit.SECONDS);
    }

    /**
     * 算法执行值
     * @param paramNo
     * @param hashslot
     * @return
     */
    public static Long hashSlotToModValue(Long paramNo, int hashslot){
        return Objects.requireNonNull(paramNo,"paramNo is not null!") & ((hashslot)-1);
    }

    /**
     * 计算hash值
     * @param paramNo
     * @param hashslot
     * @return
     */
    public static Long hashSlotToValueComp(Long paramNo,int hashslot){
        Objects.requireNonNull(paramNo,"paramNo is not null!");
        while(!(((hashslot - 1) & hashslot) == 0 )){
            hashslot++;
        }
        log.debug("the hash slot value:{}",hashslot);
        return hashSlotToModValue(paramNo,hashslot);
    }

    /**
     * hash算法计算模运算机制
     * @param paramNo
     * @return
     */
    public static String hashModTransferPartition(String prefixName,Long paramNo,int hashslot){
        // 必须是2的n次方
        long hashslotValue = hashSlotToValueComp(paramNo,hashslot);
        return String.format(prefixName+"%s",hashslotValue);
    }

    /**
     * 自动分区操作控制
     * @param partionPrefix 队列前缀
     * @param entity
     * @param <T>
     * @return
     */
    public <T> Long addAutoPartitionBlockingElement(String partionPrefix,HashQueueElement<T> entity,int hashSlotNum){
        //2 个 hash母槽 算法机制 会直接衍生为 4 个 hash槽
        Preconditions.checkArgument(StringUtils.isNotBlank(partionPrefix),"partitionPrefix is not null!");
        Preconditions.checkNotNull(entity,"entity is not null!");
        //将同一个类型下的数据作为同一个队列的数据信息，串行处理机制。
        String key = hashModTransferPartition(partionPrefix,entity.getHashResourceId(),hashSlotNum);
        return addBlockingElement(key, entity.getElement());
    }

    /**
     * 充分发挥CPU能力，伴随这核心数据越多，划分的数据越多
     * @param partionPrefix
     * @param entity
     * @param <T>
     * @return
     */
    public <T> Long addAutoPartitionBlockingElement(String partionPrefix,HashQueueElement<T> entity){
        return addAutoPartitionBlockingElement(partionPrefix,entity,CURRENT_EXECUTE_DO_SIZE);
    }

    /**
     * 获取相关的数据信息
     * @param partionPrefix 队列队列分区前缀
     * @param hashResourceId hash算法的输入条件值
     * @return
     */
    public <T> Long pollAutoPartitionBlockingElement(String partionPrefix,Long hashResourceId){
        return pollAutoPartitionBlockingElement(partionPrefix,hashResourceId,CURRENT_EXECUTE_DO_SIZE);
    }

    /**
     * 获取相关的数据信息
     * @param partionPrefix 队列队列分区前缀
     * @param hashResourceId hash算法的输入条件值
     * @param hashSlotNum 手动指定hash slot值数据
     * @return
     */
    public <T> T pollAutoPartitionBlockingElement(String partionPrefix,Long hashResourceId,int hashSlotNum){
        //2 个 hash母槽 算法机制 会直接衍生为 4 个 hash槽
        Preconditions.checkArgument(StringUtils.isNotBlank(partionPrefix),"partitionPrefix is not null!");
        Preconditions.checkNotNull(hashResourceId,"hashResourceId is not null!");
        String key = hashModTransferPartition(partionPrefix,hashResourceId,hashSlotNum);
        Long expireTime = redisTemplate.getExpire(key, TimeUnit.SECONDS);
        if(Objects.nonNull(expireTime) && expireTime < 60){
            log.debug("key:{}  ttl  {} is less than 60 seconds will be update ttl to long",key,expireTime);
            redisTemplate.expire(key, 5, TimeUnit.HOURS);
        }
        try {
            T result = (T) redisTemplate.opsForList().rightPop(key, 3, TimeUnit.SECONDS);
            if(Objects.isNull(result)) {
                redisTemplate.opsForHash().increment(HASH_SEQ_NUMBER_2, key, 1L);
            }
            return result;
        } catch (Exception e) {
            log.error("remove hash counter is failure!",e);
            return null;
        }
    }



    /**
     * get阻塞队列元素控制机制
     * @param partionPrefix 前缀编码
     * @param hashSlotNum hashslot值
     */
    public <T> T pollPartitionBlockingElement(String partionPrefix, int hashSlotNum) {
        String key = partionPrefix+hashSlotNum;
        Long expireTime = redisTemplate.getExpire(key, TimeUnit.SECONDS);
        if(Objects.nonNull(expireTime) && expireTime < 60){
            log.debug("key:{}  ttl  {} is less than 60 seconds will be update ttl to long",key,expireTime);
            redisTemplate.expire(key, 5, TimeUnit.HOURS);
        }
        try {
            T result =  (T) redisTemplate.opsForList().rightPop(key, 5, TimeUnit.SECONDS);
            if(Objects.nonNull(result)){
                redisTemplate.opsForHash().increment(HASH_SEQ_NUMBER_2,key,1L);
            }
            return result;
        } catch (Exception e) {
            log.error("remove hash counter is failure!",e);
            return null;
        }
    }


    /**
     * 启动所有针对于hashslot的线程执行器
     * @param workerName
     * @param hashSlotNum
     */
    public ExecutorService initPollExecuteWorker(String workerName, int hashSlotNum, Consumer runnable,ExecutorService executorService){
        ExecutorService threadPoolTaskExecutor = executorService;
        IntStream.range(0, hashSlotNum).forEach(param -> {
            log.info("bootstrap worker {}{}", workerName,param);
            threadPoolTaskExecutor.execute(new Thread(() -> executeFinishData(param,workerName,runnable),
                    workerName.concat(String.valueOf(param))));
        });
        return threadPoolTaskExecutor;
    }

    /**
     * 初始化线程池指定的数据线程池
     * @param workerName
     * @param runnable
     * @param executorService
     */
    public ExecutorService initPollExecuteWorker(String workerName, Consumer runnable,ExecutorService executorService){
        return initPollExecuteWorker(workerName,CURRENT_EXECUTE_DO_SIZE,runnable, executorService);
    }

    /**
     * 初始化线程池指定相应的线程池
     * @param workerName
     * @param hashSlotNum
     * @param runnable
     */
    public ExecutorService initPollExecuteWorker(String workerName, int hashSlotNum, Consumer runnable){
        return initPollExecuteWorker(workerName,hashSlotNum,runnable,ThreadUtil.newExecutor(hashSlotNum,hashSlotNum+CURRENT_EXECUTE_DO_SIZE,
                200));
    }

    /**
     * 启动所有针对于hashslot的线程执行器
     * @param partionPrefix
     * @param runnable
     */
    public ExecutorService initPollExecuteWorker(String partionPrefix, Consumer runnable){
        return initPollExecuteWorker(partionPrefix,CURRENT_EXECUTE_DO_SIZE,runnable);
    }


    /**
     * 执行数据迁移操作控制机制
     */
    public void executeFinishData(int param, String queuePrefix, Consumer runnable){
        //循环获取
        while(true){
            try {
                Object dataElement =
                        pollPartitionBlockingElement(queuePrefix,param);
                if(Objects.isNull(dataElement)){
                    try {
                        Thread.sleep(2000);
                        // 暂时交出使用权
//                        Thread.yield();
                    } catch (InterruptedException e) {
                        log.error("deal with process failure InterruptedException",e);
                    }
                }else {
                    log.info("queue read element data : {} ", dataElement);
                    runnable.accept(dataElement);
                    log.info("deal with process finished");
                }
            } catch (Exception e) {
                log.error("deal with process failure",e);
            }
        }
    }
}
