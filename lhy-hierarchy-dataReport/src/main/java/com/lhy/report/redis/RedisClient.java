/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.redis;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisConnectionUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.stereotype.Service;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.redis
 * @author:LiBo/Alex
 * @create-date:2021-10-18 10:31 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Slf4j
@Service
public class RedisClient {

    @Autowired
    private RedisTemplate<Object, Object> redisTemplate;



    private RedisConnection getConnection() {
        return redisTemplate.getConnectionFactory().getConnection();
    }

    /**
     * 释放连接
     * @param redisConnection
     */
    private void releaseConnection(RedisConnection redisConnection){
        if(null!=redisConnection && null!=redisTemplate){
            RedisConnectionFactory redisConnectionFactory = redisTemplate.getConnectionFactory();
            if(null!=redisConnectionFactory){
                RedisConnectionUtils.releaseConnection(redisConnection, redisConnectionFactory);
            }
        }
    }

    /**
     * 获取缓存的key
     * @param id
     * @return
     */
    private <T> byte[] getKey(T id) {
        RedisSerializer serializer = redisTemplate.getKeySerializer();
        return serializer.serialize(id);
    }

    /**
     * 更新缓存中的对象，也可以在redis缓存中存入新的对象
     *
     * @param key
     * @param t
     * @param <T>
     */
    public <T> void set(String key, T t) {
        byte[] keyBytes = getKey(key);
        RedisSerializer serializer = redisTemplate.getValueSerializer();
        byte[] val = serializer.serialize(t);
        RedisConnection redisConnection = getConnection();

        if(null!=redisConnection){
            try {
                redisConnection.set(keyBytes, val);
            }finally {
                releaseConnection(redisConnection);
            }
        }else{
            log.error("1. can not get valid connection");
        }
    }

    /**
     * 删除指定对象
     * @param key
     * @return
     */
    public long del(String key){
        RedisConnection redisConnection = getConnection();
        long rlt = 0L;

        if(null!=redisConnection){
            try {
                rlt = redisConnection.del(getKey(key));
            }finally {
                releaseConnection(redisConnection);
            }
        }else{
            log.error("1. can not get valid connection");
        }
        return rlt;
    }

    /**
     * 从缓存中取对象
     *
     * @param key
     * @param <T>
     * @return
     */
    public <T> T getObject(String key) {
        byte[] keyBytes = getKey(key);
        byte[] result = null;
        RedisConnection redisConnection = getConnection();
        if(null!=redisConnection){
            try {
                result = redisConnection.get(keyBytes);
            }finally {
                releaseConnection(redisConnection);
            }
        }else{
            log.error("2. can not get valid connection");
        }
        return null!=redisConnection ? (T) redisTemplate.getValueSerializer().deserialize(result) : null;
    }
}
