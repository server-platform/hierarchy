/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.delayed.context;

import com.lhy.report.bean.delayed.ExecuteInvokerEvent;
import com.lhy.report.delayed.impl.EventExecutableInvokerListener;
import com.lhy.report.delayed.listener.ExecutableExceptionHandler;
import com.lhy.report.delayed.redis.DelayedRedissionClientTool;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RBlockingQueue;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.Executor;

/**
 * @project-name:wiz-shrding-framework
 * @package-name:com.wiz.sharding.framework.boot.starter.redisson.delayed.context
 * @author:LiBo/Alex
 * @create-date:2021-08-11 16:34
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */

@RequiredArgsConstructor
@Slf4j
public  class DelayedBootstrapRunnable implements Runnable{



    /**
     * 直接传递相关的执行客户端访问器
     */
    public DelayedRedissionClientTool delayedRedissionClientTool = DelayedRedisClientSupport.getDelayedRedissionClientTool();


    /**
     * 绑定的线程组，只会执行相关的线程组之间的关系机制
     */
    public final String bizGroup;

    /**
     * 注入参数进入
     */
    public final List<EventExecutableInvokerListener> eventExecutableInvokerListeners;

    /**
     * 执行线程池
     */
    public final Executor executorThreadPool;


    /**
     * 异常信息控制
     */
    public final ExecutableExceptionHandler exceptionHandlers;


    /**
     * 启动服务处理机制
     */
    @Override
    public void run() {
        try {
            RBlockingQueue<ExecuteInvokerEvent> blockingQueue = delayedRedissionClientTool.takeBlockingQueue(new ExecuteInvokerEvent(bizGroup));
            Executor executor = Objects.isNull(executorThreadPool) ? DelayedThreadPoolSupport.getTaskExecuteThread() : executorThreadPool;
            Thread.currentThread().setUncaughtExceptionHandler(exceptionHandlers);
            for(;;) {
                ExecuteInvokerEvent data =  delayedRedissionClientTool.poll(blockingQueue);
                log.debug("Listening Queue task group：{},Get value:{}", bizGroup, data);
                executor.execute(() -> {
                    for(EventExecutableInvokerListener eventExecutableInvokerListener : eventExecutableInvokerListeners){
                        eventExecutableInvokerListener.handle(data);
                    }
                });
//                throw new RuntimeException("asdasdas");
            }
        } catch (Exception e) {
            log.error("Failed to execute processing！",e);
//            throw new RuntimeException(e);
        }
    }



}
