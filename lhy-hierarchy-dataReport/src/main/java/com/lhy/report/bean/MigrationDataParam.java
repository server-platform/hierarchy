/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.bean;

import cn.hutool.core.clone.CloneSupport;
import com.lhy.report.tools.DateRangeSplitter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @project-name:wiz-sound-ai3
 * @package-name:com.wiz.soundai.task.domain.entity.migration
 * @author:LiBo/Alex
 * @create-date:2021-09-16 19:18
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@NoArgsConstructor
@Getter
@Setter
public class MigrationDataParam extends CloneSupport<MigrationDataParam> implements Serializable{

    /******************计算数据查询条件************************************/
    /**
     * 计算总开始时间
     */
    private LocalDateTime computeStartTime;

    /**
     * 计算总结束时间
     */
    private LocalDateTime computeEndTime;

    /**
     * 指定目标的业务编码值
     */
    private List<Long> targetBizCodes;

    /**
     * 是否平衡数据执行速度
     */
    private boolean balanceRate = Boolean.TRUE;

    /**
     * 处理速度值
     */
    private int processRateValue = 5000;

    /**
     * 业务数据状态筛选
     */
    private Byte dataStatus;


    /**
     * 业务数据类型筛选
     */
    private Byte dataType;


    /**
     * 是否真正进行执行
     */
    private boolean mockOperation = Boolean.TRUE;

    /**
     * 需要执行的总数
     */
    private Long allCount = 0L;


    /******************内部认识时间分片机制************************************/

    private DateRangeSplitter.DateRangeSplit dateRangeSplit;


    private Date fetchStartTime;


    private Date fetchEndTime;

    /**
     * 时间间隔的天数分片机制
     */
    private Integer dateSplitIntervalDays;


    /******************内部数据范围分片机制************************************/

    /**
     * 需要处理的数据需要处理的游标计算
     */
    private Long dataCursor ;

    /**
     * 需要处理的数据单元-默认与processRateValue相同
     */
    private Long processDataUnit = processRateValue+0L;

    /**
     * 获取自身的数据信息
     * @return
     */
    public Date getFetchStartTime() {
        if(Objects.nonNull(fetchStartTime)){
            return fetchStartTime;
        }
        if(MigrationDataRangeType.MIGRATION_TARGET_IDS == dataRangeType || MigrationDataRangeType.MIGRATION_DATA_RANGE == dataRangeType){
            return null;
        }
        Objects.requireNonNull(dateRangeSplit,"dateRangeSplit must be null");
        return dateRangeSplit.getSplitStartDate();
    }

    /**
     * 获取自身的数据信息
     * @return
     */
    public Date getFetchEndTime() {
        if(Objects.nonNull(fetchEndTime)){
            return fetchEndTime;
        }
        if(MigrationDataRangeType.MIGRATION_TARGET_IDS == dataRangeType || MigrationDataRangeType.MIGRATION_DATA_RANGE == dataRangeType){
            return null;
        }
        Objects.requireNonNull(dateRangeSplit,"dateRangeSplit must be null");
        return dateRangeSplit.getSplitEndDate();
    }


    /******************执行操作类型机制************************************/

    private MigrationDataRangeType dataRangeType;

    /**
     * 迁移数据的时间范围
     */
    public enum MigrationDataRangeType{

        /**
         * 迁移数据指定的ID集合信息
         */
        MIGRATION_TARGET_IDS,

        /**
         * 迁移数据指定的时间范围
         */
        MIGRATION_DATE_RANGE,

        /**
         * 迁移数据指定的数据范围
         */
        MIGRATION_DATA_RANGE,

        /**
         * 迁移数据补偿操作
         */
        MIGRATION_DATA_RECOVERY;

    }

    public MigrationDataParam(Date fetchStartTime, Date fetchEndTime) {
        this.fetchStartTime = fetchStartTime;
        this.fetchEndTime = fetchEndTime;
    }

    public MigrationDataParam(List<Long> targetBizCodes) {
        this.targetBizCodes = targetBizCodes;
    }

    /**
     * 清理获取的时间数据范围
     */
    public void clearFetchDateRange(){
        this.fetchEndTime = null;
        this.fetchStartTime = null;
    }

    public MigrationDataParam(Byte dataType) {
        this.dataType = dataType;
    }


    public MigrationDataParam(Byte dataType, Date fetchStartTime, Date fetchEndTime) {
        this.dataType = dataType;
        this.fetchStartTime = fetchStartTime;
        this.fetchEndTime = fetchEndTime;
    }


    private Date recoveryStartTime;


    private Date recoveryEndTime;

    @Override
    public String toString() {
        return "MigrationDataParam{" + "computeStartTime=" + computeStartTime + ", computeEndTime=" + computeEndTime + ", targetBizCodes=" + targetBizCodes + ", balanceRate=" + balanceRate + ", processRateValue=" + processRateValue + ", dataStatus=" + dataStatus + ", dataType=" + dataType + ", mockOperation=" + mockOperation + ", allCount=" + allCount + ", dateRangeSplit=" + dateRangeSplit + ", fetchStartTime=" + fetchStartTime + ", fetchEndTime=" + fetchEndTime + ", dataRangeType=" + dataRangeType + '}';
    }
}
