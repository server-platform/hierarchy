/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.bean;

import lombok.Data;
import lombok.NonNull;

/**
 * @project-name:wiz-sound-ai2
 * @package-name:com.wiz.soundai.task.domain.entity.migration
 * @author:LiBo/Alex
 * @create-date:2021-09-26 11:36
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Data
public class MigrationProcessResponse<T>{

    /**
     * 存入到redis或者下游队列中的数据信息模型
     */
    @NonNull
    private String outputStreamSlinkName;

    /**
     * 数据模型信息
     */
    @NonNull
    private HashQueueElement entity;

}
