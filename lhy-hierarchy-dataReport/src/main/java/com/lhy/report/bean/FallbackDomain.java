/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.bean;

import lombok.*;

import java.io.Serializable;

/**
 * @project-name:wiz-sound-ai6
 * @package-name:com.wiz.soundai.callservice.gateway.domain
 * @author:LiBo/Alex
 * @create-date:2021-11-16 13:37
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@SuppressWarnings("serial")
public class FallbackDomain implements Serializable {


    private String uuid;


    private String processType;


    private Object model;

    /**
     * 是否执行process操作 默认为TRUE ， 作为开关扩展,一般在重试的时候，可以将其关闭，作为我们的控制切换执行方式
     */
    private boolean execProcess = Boolean.TRUE;

    /**
     * 是否需要支持process重试操作
     */
    private boolean needRetryProcess = Boolean.FALSE;




}
