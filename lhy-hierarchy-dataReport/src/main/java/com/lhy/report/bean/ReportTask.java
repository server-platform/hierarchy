/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.bean
 * @author:LiBo/Alex
 * @create-date:2021-10-02 6:51 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */

@Data
@TableName("report_task")
public class ReportTask {


    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * spring beanName
     */
    @TableField(value = "spring_bean_name")
    private String springBeanName;


    @TableField(value = "cron_expression")
    private String cronExpression;

}
