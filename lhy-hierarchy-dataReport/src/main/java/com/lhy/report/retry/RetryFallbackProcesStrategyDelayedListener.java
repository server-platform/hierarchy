/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.retry;

import com.lhy.report.bean.FallbackDomain;
import com.lhy.report.bean.delayed.ExecuteInvokerEvent;
import com.lhy.report.delayed.annotation.DelayedQueueListener;
import com.lhy.report.delayed.impl.EventExecutableInvokerListener;
import com.lhy.report.fallback.imol.DefaultRetryFallbackProcessStrategy;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * @project-name:wiz-sound-ai6
 * @package-name:com.wiz.soundai.callservice.gateway.manager.delayed
 * @author:LiBo/Alex
 * @create-date:2021-11-16 13:14
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description: 重试且fallback的功能机制处理控制
 */


@Slf4j
@DelayedQueueListener(value="retryFallbackProcesStrategyDelayedListener",group = DefaultRetryFallbackProcessStrategy.DEFAULT_RETRY_EXECUTE_TASK_QUEUE)
public class RetryFallbackProcesStrategyDelayedListener implements EventExecutableInvokerListener<FallbackDomain,Object> {


    /**
     * 处理独立的线程池处理相关的失败处理操作
     * @return
     */
    @Override
    public Executor getExecutor() {
        return new ThreadPoolExecutor(10,10,60, TimeUnit.SECONDS,new ArrayBlockingQueue<>(200));
    }

    /**
     * 处理相关的handle操作处理机制
     * @param executeInvokerEvent
     * @return
     */
    @Override
    public Object handle(ExecuteInvokerEvent<FallbackDomain> executeInvokerEvent) {
        try {
            log.info("listener the queue to {} to execute retry",DefaultRetryFallbackProcessStrategy.DEFAULT_RETRY_EXECUTE_TASK_QUEUE);
            log.info("receive the data is {}",executeInvokerEvent);
            FallbackDomain fallbackDomain = executeInvokerEvent.getDataModel();
            // 操作处理机制控制
            log.info("retryProcess the result:{}");
            return null;
        } catch (Exception e) {
            log.error("execute the delayed queue retry is faliure!",e);
            return null;
        }
    }

}
