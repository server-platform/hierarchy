/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.write;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;

import java.util.List;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.write
 * @author:LiBo/Alex
 * @create-date:2021-09-11 11:37
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class ExcelDataWriter {


    /**
     * write写操作
     * @param excelFilePath
     * @param clazz
     */
    public static <T>void write(String excelFilePath,Class<T> clazz,int sheetIndex,String sheetName,
                                List<T> dataList,List<List<String>> head){
        ExcelWriterSheetBuilder writerSheetBuilder = EasyExcel.write(excelFilePath,clazz).autoCloseStream(Boolean.TRUE)
                .sheet(sheetIndex,sheetName);
        //  不等于空
        if(CollectionUtil.isNotEmpty(head)){
            writerSheetBuilder.head(head).automaticMergeHead(Boolean.TRUE).needHead(Boolean.TRUE);
        }
        writerSheetBuilder.autoTrim(Boolean.TRUE).useDefaultStyle(Boolean.TRUE).doWrite(dataList);
    }




}
