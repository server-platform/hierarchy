/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.logic;

import com.lhy.report.bean.ReportTask;
import com.lhy.report.dao.ReportTaskDao;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.logic
 * @author:LiBo/Alex
 * @create-date:2021-10-02 6:58 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Slf4j
@Service
public class ReportTaskLogic {

    @Autowired
    private ReportTaskDao reportTaskDao;


    /**
     * getCronExpression
     * @return
     */
    public String getCron() {
        // 只是为了测试 固定id
        ReportTask reportTask = reportTaskDao.selectById(1);
        return (reportTask).getCronExpression();
    }


}
