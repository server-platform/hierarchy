/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.client;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.client
 * @author:LiBo/Alex
 * @create-date:2021-10-04 10:33 AM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public interface GithubClient {


    @ToString
    @Data
    class Contributor {
        String login;
        int contributions;
    }

    /**
     * RequestLine注解声明请求方法和请求地址,可以允许有查询参数
     * @param owner
     * @param repo
     * @return
     */
    @Headers("Accept: application/json")
    @RequestLine("GET /repos/{owner}/{repo}/contributors")
    List<Contributor> contributors(@Param("owner") String owner, @Param("repo") String repo);
}
