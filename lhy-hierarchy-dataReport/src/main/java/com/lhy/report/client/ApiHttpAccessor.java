/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.client;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.client
 * @author:LiBo/Alex
 * @create-date:2021-10-04 10:32 AM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class ApiHttpAccessor {

    /**
     * 获取相关的服务机制控制
     * @return
     */
    public GithubClient getGithubClient(){
        return Feign.builder()
                .logger(new Slf4jLogger())
//        HystrixFeign.builder()
                .encoder(new GsonEncoder())
                .client(new OkHttpClient())
                .decoder(new GsonDecoder())
                .target(GithubClient.class, "https://api.github.com");
    }


    public static void main(String[] args){
        System.out.println(new ApiHttpAccessor().getGithubClient().contributors("liboware","quartz"));
    }

}
