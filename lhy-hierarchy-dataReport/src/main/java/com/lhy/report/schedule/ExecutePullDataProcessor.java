/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.schedule;

import com.lhy.report.redis.HashRedisPartitionQueueManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.task
 * @author:LiBo/Alex
 * @create-date:2021-11-13 11:54 上午
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Component
public class ExecutePullDataProcessor {


    @Autowired
    HashRedisPartitionQueueManager hashRedisPartitionQueueManager;



   // @PostConstruct
    public void initExecutePullData(){
        hashRedisPartitionQueueManager.initPollExecuteWorker("EXECUTE_PROCESS:",
                4,param->{});
    }


}
