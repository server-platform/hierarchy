/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.schedule.execute;

import com.lhy.report.bean.ReportTask;
import org.springframework.scheduling.support.CronTrigger;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.task.execute
 * @author:LiBo/Alex
 * @create-date:2021-10-02 7:06 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public class ReportTasktExecuteTask extends CronTrigger {


    public ReportTasktExecuteTask(ReportTask reportTask) {
        super(reportTask.getCronExpression());
    }

}
