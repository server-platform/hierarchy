/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.schedule;

import com.lhy.report.schedule.execute.HeartBeatExecuteTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.task
 * @author:LiBo/Alex
 * @create-date:2021-10-02 6:45 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
//@EnableScheduling
@Configuration
public class DynamicTaskScheduler implements SchedulingConfigurer {


    @Autowired
    ApplicationContext applicationContextAware;


    @Autowired
    HeartBeatExecuteTask executeTask;


    private ScheduledTaskRegistrar scheduledTaskRegistrar;


    @Override
    public void configureTasks(ScheduledTaskRegistrar scheduledTaskRegistrar) {
        scheduledTaskRegistrar.addCronTask(executeTask,"*/1 * * * * ?");
        this.scheduledTaskRegistrar = scheduledTaskRegistrar;
    }



    public void addReportTask(Runnable supplier,String expression){
        scheduledTaskRegistrar.addCronTask(supplier,expression);
    }

}
