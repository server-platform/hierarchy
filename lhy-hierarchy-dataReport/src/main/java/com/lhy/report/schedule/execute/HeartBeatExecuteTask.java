/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.schedule.execute;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.report.task.execute
 * @author:LiBo/Alex
 * @create-date:2021-10-02 7:14 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Component
@Slf4j
public class HeartBeatExecuteTask implements Runnable {


    @Autowired
    ApplicationContext applicationContextAware;

    @Value("${version:true}")
    private Boolean version;


    @Autowired
    RedisTemplate redisTemplate;


    @Override
    public void run() {
        if(version) {
            System.out.println(redisTemplate.opsForList().rightPop("test"));
        }
    }
}
