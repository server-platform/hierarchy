/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.event;

import cn.hutool.core.thread.ExecutorBuilder;
import com.lhy.report.read.event.AsyncEventModelListener;
import com.lmax.disruptor.*;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.event
 * @author:LiBo/Alex
 * @create-date:2021-09-25 00:36
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Slf4j
@Scope("prototype")
@RequiredArgsConstructor
@Component
public class EventModelManager<T> implements AutoCloseable {

    /**
     * disruptor事件处理器
     */
    @Setter
    @Getter
    private Disruptor<EventModel<T>> disruptor;

    /**
     * 事件处理链表
     */
    @NonNull
    private final List<EventHandler<EventModel<T>>> eventHandlers;


    public Translator TRANSLATOR = new Translator();

    /**
     * RingBuffer 大小，必须是 2 的 N 次方；
     */
    private final int DEFAULT_RING_BUFFER_SIZE = 1024 * 1024;

    /**
     * 事件工厂类
     */
    private EventFactory<EventModel<T>> eventFactory = new EventModelFactory<>();

    /**
     * 线程工厂类
     */
    private ThreadFactory threadFactory =  r -> new Thread(r,"EventModelManager"+System.currentTimeMillis());


    /**
     * EventFactory构造器服务机制
     */
    @SuppressWarnings("all")
    @PostConstruct
    public void init(){
        // 构造器实际线程池
        disruptor = new Disruptor<>(eventFactory, DEFAULT_RING_BUFFER_SIZE,
                threadFactory, ProducerType.SINGLE, new BlockingWaitStrategy());
        disruptor.handleEventsWith(eventHandlers.toArray(new AsyncEventModelListener[eventHandlers.size()]));
        //顾名思义：执行完t1后执行t2。（对同一个任务线性执行）
//        disruptor.after(t1).handleEventsWith(t2)。
        disruptor.start();
    }


    /**
     * publish 发布事件数据对象模型
     */
    @SuppressWarnings("all")
    public void publishEvent(T... eventModels){
        Objects.requireNonNull(disruptor,"当前disruptor核心控制器不可以为null！");
        Objects.requireNonNull(eventModels,"当前eventModels事件控制器不可以为null！");
        // 发布事件；
        RingBuffer ringBuffer = disruptor.getRingBuffer();
        try {
            //获取要通过事件传递的业务数据；
            List<T> dataList = Arrays.stream(eventModels).collect(Collectors.toList());
            for(T element:dataList){
                //请求下一个事件序号；
                long sequence = ringBuffer.next();
                //获取该序号对应的事件对象；
                EventModel event = (EventModel) ringBuffer.get(sequence);
                event.setEntity(element);
                ringBuffer.publish(sequence);
            }
        }catch (Exception e){
            log.error("error",e);
        };
    }


    /**
     * 关闭操作处理机制
     * @throws Exception
     */
    @Override
    public void close() throws Exception {
        if(Objects.nonNull(disruptor)){
            disruptor.shutdown();
        }
    }

    /**
     * 转换器模型
     */
    public class Translator implements EventTranslatorOneArg<EventModel<T>, T>{
        @Override
        public void translateTo(EventModel<T> event, long sequence, T data) {
            event.setEntity(data);
        }
    }


    /**
     * 发送事件模型
     */
    @SuppressWarnings("all")
    public void sendEvent(T... events){
       // 注意，最后的 ringBuffer.
       // publish 方法必须包含在 finally 中以确保必须得到调用；如果某个请求的 sequence 未被提交，将会堵塞后续的发布操作或者其它的 producer。
       // Disruptor 还提供另外一种形式的调用来简化以上操作，并确保 publish 总是得到调用。
        RingBuffer ringBuffer = disruptor.getRingBuffer();
        //获取要通过事件传递的业务数据；
        for(T event:events){
            ringBuffer.publishEvent(TRANSLATOR,event);
        }
    }


    /**
     * 线程工厂类
     * @return
     */
    public static ThreadFactory createThreadFactory(){
        return  r -> new Thread(r,"EventModelManager"+System.currentTimeMillis());
    }

    /**
     * 创建执行器
     * @return
     */
    public static Executor createExecutor(){
        return  ExecutorBuilder.create().setCorePoolSize(Runtime.getRuntime().availableProcessors()).
                setMaxPoolSize(100).setKeepAliveTime(60, TimeUnit.SECONDS).setWorkQueue(new ArrayBlockingQueue(200)).
                setThreadFactory(createThreadFactory()).setHandler(new
                ThreadPoolExecutor.DiscardOldestPolicy()).build();
    }


}
