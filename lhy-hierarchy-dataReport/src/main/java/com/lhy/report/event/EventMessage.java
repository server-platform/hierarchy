/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.event;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.event
 * @author:LiBo/Alex
 * @create-date:2021-10-02 12:41 PM
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@AllArgsConstructor
@Data
public class EventMessage<T> {

    private T data;

}
