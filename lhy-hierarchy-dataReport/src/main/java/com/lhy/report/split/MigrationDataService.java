/**
 * Copyright [2019] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.split;

import com.lhy.report.bean.MigrationDataParam;

import java.util.concurrent.ExecutorService;

/**
 * @project-name:wiz-sound-ai3
 * @package-name:com.wiz.soundai.task.service
 * @author:LiBo/Alex
 * @create-date:2021-09-18 9:46
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
public interface MigrationDataService {


    /**
     * 通过任务条件进行获取相关迁移的呼叫记录服务接口信息
     */
    void migrationDataProcess(MigrationDataParam migrationCallRecordParam);


    /**
     * 结束数据信息集合方法
     * @param migrationCallRecordParam
     */
    void finalizeProcess(MigrationDataParam migrationCallRecordParam);


    /**
     * 自定义线程池
     * @return
     */
    ExecutorService getExecutorService();
}
