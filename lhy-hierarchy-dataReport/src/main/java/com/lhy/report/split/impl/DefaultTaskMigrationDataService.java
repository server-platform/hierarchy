/**
 * Copyright [2020] [LiBo/Alex of copyright liboware@gmail.com ]
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package report.split.impl;

import cn.hutool.core.thread.ThreadUtil;
import com.google.common.collect.Lists;
import com.lhy.report.bean.MigrationDataParam;
import com.lhy.report.bean.MigrationProcessResponse;
import com.lhy.report.redis.HashRedisPartitionQueueManager;
import com.lhy.report.split.AbstractMigrationDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;

/**
 * @project-name:lhy-report
 * @package-name:com.lhy.lhy.report.split.impl
 * @author:LiBo/Alex
 * @create-date:2021-09-22 22:57
 * @copyright:libo-alex4java
 * @email:liboware@gmail.com
 * @description:
 */
@Component
@Slf4j
public class DefaultTaskMigrationDataService extends AbstractMigrationDataService {



    ExecutorService executorService = null;



    @Override
    protected List fetchDataFunction(MigrationDataParam migrationCallRecordParamSub) {
        List<String> dataList = Lists.newArrayListWithExpectedSize(100000);
        IntStream.rangeClosed(0,50000).forEach(param->{
            dataList.add(String.valueOf(param));
        });
        return dataList;
    }



    @Override
    protected Long fetchDataAllCount(MigrationDataParam migrationCallRecordParamSub) {
        return 50000L;
    }



    @Override
    protected MigrationProcessResponse executeDataFunction(MigrationDataParam migrationCallRecordParam,
                                                           List dataList, Object entity, AtomicInteger success, AtomicInteger failure) {
        return new MigrationProcessResponse("EXECUTE_PROCESS:", HashRedisPartitionQueueManager.
                createHashQueueElement(Long.valueOf(String.valueOf(entity)),entity));
    }


    @Override
    public void finalizeProcess(MigrationDataParam migrationCallRecordParam) {
        try {
            if(Objects.nonNull(executorService)) {
                executorService.shutdown();
                executorService = null;
            }
            log.info("release the thread pool ");
        } catch (Exception e) {
            log.error("finished process info data process is failure!",e);
        }
    }


    /**
     * 自定义线程池机制 提交给框架使用！
     * @return
     */
    @Override
    public ExecutorService getExecutorService() {
        executorService = Objects.isNull(executorService) || executorService.isShutdown() || executorService.isTerminated()?
                ThreadUtil.newExecutor(10, 10, 200):executorService;
        return executorService;
    }

}
